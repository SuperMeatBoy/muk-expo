require('dotenv').config()
const path = require('path')
const withImages = require('next-images')

const nextConfig = {
  webpack: (config) => {
    config.resolve.modules.push(path.resolve('./'))
    return config
  },
  sassOptions: {
    includePaths: [path.join(__dirname, 'styles')],
  },
  publicRuntimeConfig: {
    localeSubpaths:
      typeof process.env.LOCALE_SUBPATHS === 'string'
        ? process.env.LOCALE_SUBPATHS
        : 'all',
  },
  env: {
    MONGODB_URI: process.env.MONGODB_URI,
    DB_NAME: process.env.DB_NAME,
    WEB_URI: process.env.WEB_URI,
    EMAIL_FROM: process.env.EMAIL_FROM,
  },
}

module.exports = withImages(nextConfig)
