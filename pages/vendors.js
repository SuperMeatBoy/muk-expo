import React from 'react'
import { Layout } from 'components/layout'
import { withTranslation } from 'utils/with-i18next'
import Breadcrumbs from 'components/breadcrumbs'
import { VendorPreview } from 'components/vendors'
import { vendors } from 'utils/data.js'
import FormITConsultant from 'components/form/itConsultant'
import { SearchBlock } from 'components/form'

const VendorCatalog = ({ t }) => {
  const content = [
    ...vendors
      .sort((a, b) => a.title.localeCompare(b.title))
      .reduce((acc, element) => {
        const letter = element.title.charAt(0)
        if (acc.has(letter)) {
          acc.get(letter).push(element)
        } else {
          acc.set(letter, [element])
        }
        return acc
      }, new Map())
      .values(),
  ]

  return (
    <Layout>
      <section className='catalog-vendors pb-5'>
        <div className='container'>
          <Breadcrumbs data={[{ name: 'Вендоры' }]} />
          <h1 className='title title__section text-white pb-4'>
            {t('Каталог вендоров')}
          </h1>
          <SearchBlock />
        </div>
      </section>
      <div className='container mt-5'>
        {content.map((item, index) => {
          return (
            <div className='row mt-2' key={index}>
              <div className='col-12 mb-4'>
                <strong>{item[0].title.charAt(0)}</strong>
              </div>
              {item.map((elem, ind) => (
                <div key={ind} className='col-md-4 col-sm-6 col-12'>
                  <VendorPreview {...elem} />
                </div>
              ))}
            </div>
          )
        })}
      </div>
      <div className='container mt-5'>
        <div className='row'>
          <FormITConsultant t={t} />
        </div>
      </div>
    </Layout>
  )
}

VendorCatalog.getInitialProps = async () => ({
  namespacesRequired: ['common'],
})

export default withTranslation('common')(VendorCatalog)
