import React from 'react'
import { Layout } from 'components/layout'
import { withTranslation } from 'utils/with-i18next'
import Breadcrumbs from 'components/breadcrumbs'
import FormITConsultant from 'components/form/itConsultant'
import { useRouter } from 'next/router'
import { products } from 'utils/data'
import { Categories } from 'components/category-list'

import logoVendor from 'public/images/fake-data/amd-logo.png'

const Vendor = (props) => {
  const router = useRouter()
  const { t, id } = props

  return (
    <Layout header='white'>
      <section className='vendor pb-5'>
        <div className='container'>
          <Breadcrumbs
            mode='white'
            data={[{ name: 'Вендоры', link: '/vendors' }, { name: 'AMD' }]}
          />
          <h1 className='title text-dark'>Продукция компании AMD</h1>
        </div>
      </section>
      <div className='container'>
        <div className='row mb-4'>
          <div className='col-sm-6'>
            <img className='img-responsive' src={logoVendor} alt='' />
            <a className='link d-block mt-5' href='#'>
              AMD
            </a>
          </div>
          <div className='col-sm-6'>
            <p className='text'>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean
              euismod bibendum laoreet. Proin gravida dolor sit amet lacus
              accumsan et viverra justo commodo. Proin sodales pulvinar sic
              tempor. Sociis natoque penatibus et magnis dis parturient montes,
              nascetur ridiculus mus. Nam fermentum, nulla luctus pharetra
              vulputate, felis tellus mollis orci, sed rhoncus pronin sapien
              nunc accuan eget.
              <br />
              Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean
              euismod bibendum laoreet. Proin gravida dolor sit amet lacus
              accumsan et viverra justo commodo. Proin sodales pulvinar sic
              tempor. Sociis natoque penatibus et magnis dis parturient montes,
              nascetur ridiculus mus. Nam fermentum, nulla luctus pharetra
              vulputate, felis tellus mollis orci, sed rhoncus pronin sapien
              nunc accuan eget.
              <br />
              Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean
              euismod bibendum laoreet. Proin gravida dolor sit amet lacus
              accumsan et viverra justo commodo. Proin sodales pulvinar sic
              tempor. Sociis natoque penatibus et magnis dis parturient montes,
              nascetur ridiculus mus. Nam fermentum, nulla luctus pharetra
              vulputate, felis tellus mollis orci, sed rhoncus pronin sapien
              nunc accuan eget.
            </p>
          </div>
        </div>
        <div className='line' />
      </div>
      <div className='container mb-4'>
        <Categories list={products} />
        <div className='line mt-3' />
      </div>
      <div className='container mb-4'>
        <div className='row'>
          <div className='col-md-8 col-12'>
            <h5 className='mb-3 h5'>Адрес офиса</h5>
            <p className='text'>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean
              euismod bibendum laoreet. Proin gravida dolor sit amet lacus
              accumsan et viverra justo commodo. Proin sodales pulvinar sic
              tempor. Sociis natoque penatibus et magnis dis parturient montes,
              nascetur ridiculus mus. Nam fermentum, nulla luctus pharetra
              vulputate, felis tellus mollis orci, sed rhoncus pronin sapien
              nunc accuan eget.
            </p>
          </div>
        </div>
        <div className='line' />
      </div>
      <div className='container mb-5'>
        <div className='row'>
          <div className='col-md-8 col-12'>
            <h5 className='mb-3 h5'>Новости компании</h5>
            <p className='text'>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean
              euismod bibendum laoreet. Proin gravida dolor sit amet lacus
              accumsan et viverra justo commodo. Proin sodales pulvinar sic
              tempor. Sociis natoque penatibus et magnis dis parturient montes,
              nascetur ridiculus mus. Nam fermentum, nulla luctus pharetra
              vulputate, felis tellus mollis orci, sed rhoncus pronin sapien
              nunc accuan eget.
            </p>
          </div>
        </div>
      </div>

      <div className='container mt-5'>
        <FormITConsultant t={t} />
      </div>
    </Layout>
  )
}

export async function getServerSideProps(ctx) {
  const id = ctx.query.id
  // Pass data to the page via props
  return { props: { id } }
}

export default withTranslation('common')(Vendor)
