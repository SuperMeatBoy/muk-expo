import React from 'react'
import { Layout } from 'components/layout'
import { withTranslation } from 'utils/with-i18next'
import Breadcrumbs from 'components/breadcrumbs'
import { Button } from 'reactstrap'
import { FormVacancy } from 'components/form/formVacancy'
import { SharingButton } from 'components/sharing-button'

import advantage1 from 'public/images/vacancies/icons/advantage-1.png'
import advantage2 from 'public/images/vacancies/icons/advantage-2.png'
import advantage3 from 'public/images/vacancies/icons/advantage-3.png'

import networking from 'public/images/fake-data/networking.jpg'

const Vacancy = ({ t }) => {
  return (
    <Layout>
      <section className='big-dark vacancy text-white pb-5'>
        <div className='container'>
          <h2 className='title mb-3'>
            <span className='mb-3 d-block'>
              {t('Перестаньте тратить жизнь на нелюбимую работу')}
            </span>
            {t('Работайте в компании с мировой известностью')}
          </h2>
          <h1 className='title text-uppercase pb-5'>
            {t('IT ДИСТРИБЬЮТОР MUK')}
          </h1>
          <ul className='d-flex advantage-list flex-column flex-sm-row'>
            <li className='advantage-item d-flex d-sm-inline-block'>
              <img src={advantage1} alt='' className='img-responsive' />
              <div className='mt-3'>{t(`Зарплата выше средней по рынку`)}</div>
            </li>
            <li className='advantage-item d-flex d-sm-inline-block'>
              <img src={advantage2} alt='' className='img-responsive' />
              <div className='mt-3'>{t('Официальное трудоустройство')}</div>
            </li>
            <li className='advantage-item d-flex d-sm-inline-block'>
              <img src={advantage3} alt='' className='img-responsive' />
              <div className='mt-3'>{t('Обучаем и взращиваем экспертов')}</div>
            </li>
          </ul>
        </div>
      </section>
      <section className='section pt-4 vacancy__card'>
        <div className='container'>
          <Breadcrumbs
            data={[
              { name: 'Вакансии', link: '/vacancies/all' },
              { name: 'Программист 1С' },
            ]}
            mode='white'
          />
          <h2 className='vacancy__title mt-4 mb-4'>{t('Программист 1С')}</h2>
          <div>
            <strong>Киев, Украина</strong>
          </div>
          <div className='vacancy__number mt-2 mb-5'>Номер вакансии: П01</div>
          <div className='row mb-5'>
            <div className='col-md-6'>
              <div className='subtitle mb-3'>Описание</div>
              <p className='text'>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean
                euismod bibendum laoreet. Proin gravida dolor sit amet lacus
                accumsan et viverra justo commodo. Proin sodales pulvinar sic
                tempor. Sociis natoque penatibus et magnis dis parturient
                montes, nascetur ridiculus mus. Nam fermentum, nulla luctus
                pharetra vulputate, felis tellus mollis orci, sed rhoncus pronin
                sapien nunc accuan eget. Sociis natoque penatibus et magnis dis
                parturient montes, nascetur ridiculus mus. Nam fermentum, nulla
                luctus pharetra vulputate, felis tellus mollis orci, sed rhoncus
                pronin sapien nunc accuan eget.
              </p>
            </div>
            <div className='col-md-6'>
              <div className='subtitle mb-3'>Требования</div>
              <ul className='text'>
                <li>
                  Разработка и запуск нового функционала согласно технического
                  задания
                </li>
                <li>Оптимизация и доработка существующего функционала</li>
                <li>
                  Автоматизация существующих бизнес-процессов компании согласно
                  технического задания
                </li>
                <li>Локализация и исправление ошибок функционала</li>
                <li>Контроль корректности работы программы</li>
                <li>Обновление конфигурации 1С (УТП)</li>
              </ul>
            </div>
            <div className='col-md-6'>
              <div className='subtitle mb-3'>Обязанности</div>
              <ul className='text'>
                <li>
                  Разработка и запуск нового функционала согласно технического
                  задания
                </li>
                <li>Оптимизация и доработка существующего функционала</li>
                <li>
                  Автоматизация существующих бизнес-процессов компании согласно
                  технического задания
                </li>
                <li>Локализация и исправление ошибок функционала</li>
                <li>Контроль корректности работы программы</li>
                <li>Обновление конфигурации 1С (УТП)</li>
              </ul>
            </div>
            <div className='col-md-6'>
              <div className='subtitle mb-3'>Мы предлагаем</div>
              <ul className='text'>
                <li>
                  Разработка и запуск нового функционала согласно технического
                  задания
                </li>
                <li>Оптимизация и доработка существующего функционала</li>
                <li>
                  Автоматизация существующих бизнес-процессов компании согласно
                  технического задания
                </li>
                <li>Локализация и исправление ошибок функционала</li>
                <li>Контроль корректности работы программы</li>
                <li>Обновление конфигурации 1С (УТП)</li>
              </ul>
            </div>
          </div>
          <div className='mb-4'>
            <h2 className='subtitle mb-3'>Фотографии и адрес места работы</h2>
            <div>Адрес: Киев, ул. Мира д. 34</div>
          </div>
          <div className='d-flex flex-wrap'>
            <div className='photo'>
              <img src={networking} alt='' className='img-responsive' />
            </div>
            <div className='photo'>
              <img src={networking} alt='' className='img-responsive' />
            </div>
            <div className='photo'>
              <img src={networking} alt='' className='img-responsive' />
            </div>
            <div className='photo'>
              <img src={networking} alt='' className='img-responsive' />
            </div>
            <div className='photo'>
              <img src={networking} alt='' className='img-responsive' />
            </div>
          </div>
          <div className='row mt-5'>
            <div className='col-md-7'>
              <div className='subtitle'>
                Оставьте заявку на вакансию отправив резюме
              </div>
              <div className='my-4'>
                Мы хотим познакомиться с Вами лично, пусть пока и заочно
              </div>
              <FormVacancy t={t} />

              <Button color='link' className='p-0 my-3 text-muted'>
                {t('Вернутся на страницу вакансий')}
              </Button>
            </div>
            <div className='col-md-5'>
              <div className='subtitle mb-2'>
                Расскажите друзьям о вакансии в соц. сетях
              </div>
              <SharingButton />
            </div>
          </div>
        </div>
      </section>
    </Layout>
  )
}

Vacancy.getInitialProps = async () => ({
  namespacesRequired: ['common'],
})

export default withTranslation('common')(Vacancy)
