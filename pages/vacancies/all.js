import React from 'react'
import { Layout } from 'components/layout'
import { withTranslation } from 'utils/with-i18next'
import Breadcrumbs from 'components/breadcrumbs'
import Carousel from 'react-multi-carousel'
import {
  responsiveCard,
  staffGallery,
  countries,
  departments,
  vacanciesList,
  vacancyCategories,
  magazineGallery,
} from 'utils/data'
import { ButtonRed, Checkbox, CustomCheckbox } from 'components/form'
import { Card, CardBody } from 'reactstrap'
import { VacancyCard } from 'components/vacancy-card'
import { FormVacancy } from 'components/form/formVacancy'
import { ProductModal } from 'components/modals'

import advantage1 from 'public/images/vacancies/icons/advantage-1.png'
import advantage2 from 'public/images/vacancies/icons/advantage-2.png'
import advantage3 from 'public/images/vacancies/icons/advantage-3.png'
import youtubeButton from 'public/images/vacancies/icons/youtube-play.svg'
import photoCamera from 'public/images/vacancies/icons/photo-camera.svg'

const VacancyAll = ({ t }) => {
  const sliderOptions = responsiveCard({ desktopItems: 2 })
  const sliderOptionsOne = responsiveCard()
  return (
    <Layout className='vacancy-all'>
      <section className='big-dark vacancy text-white pb-5'>
        <div className='container'>
          <h2 className='title mb-3'>
            <span className='mb-3 d-block'>
              {t('Перестаньте тратить жизнь на нелюбимую работу')}
            </span>
            {t('Работайте в компании с мировой известностью')}
          </h2>
          <h1 className='title text-uppercase pb-5'>
            {t('IT ДИСТРИБЬЮТОР MUK')}
          </h1>
          <ul className='d-flex advantage-list flex-column flex-sm-row'>
            <li className='advantage-item d-flex d-sm-inline-block'>
              <img src={advantage1} alt='' className='img-responsive' />
              <div className='mt-3'>{t(`Зарплата выше средней по рынку`)}</div>
            </li>
            <li className='advantage-item d-flex d-sm-inline-block'>
              <img src={advantage2} alt='' className='img-responsive' />
              <div className='mt-3'>{t('Официальное трудоустройство')}</div>
            </li>
            <li className='advantage-item d-flex d-sm-inline-block'>
              <img src={advantage3} alt='' className='img-responsive' />
              <div className='mt-3'>{t('Обучаем и взращиваем экспертов')}</div>
            </li>
          </ul>
        </div>
      </section>
      <section className='section pt-4'>
        <div className='container position-relative'>
          <Card className='quote p-4'>
            <CardBody>
              {t(
                'Идеальная работа - от которой не надо с устра "со скрипом" вставать под раздражающий будильник'
              )}
            </CardBody>
          </Card>
          <Breadcrumbs data={[{ name: 'Вакансии' }]} mode='white' />
          <h2 className='vacancy__title mt-5'>{t('НАЙДИТЕ ВАКАНСИЮ МЕЧТЫ')}</h2>
          <div className='row mt-3 mb-3'>
            <div className='col-md-2 col-6 d-flex mt-3 mb-3'>
              <Checkbox>Без опыта</Checkbox>
            </div>
            <div className='col-md-2 col-6 d-flex  mt-3 mb-3'>
              <Checkbox>Удаленная работа</Checkbox>
            </div>
            <div className='col-md-2 col-12 d-flex mt-3 mb-3'>
              <Checkbox>Возможность переезда</Checkbox>
            </div>
          </div>
          <div className='row mt-4 mb-5 flex-wrap'>
            <div className='col-12 mb-4'>{t('Выберите страну')}</div>
            <div className='col-md-10 col-12 d-flex flex-wrap'>
              {countries.map((item, index) => (
                <div key={index} className='mr-3'>
                  <CustomCheckbox>{item.label}</CustomCheckbox>
                </div>
              ))}
            </div>
          </div>
          <div className='row mt-5 mb-5'>
            <div className='col-12 mb-4'>{t('Выберите страну')}</div>
            <div className='col-md-10 col-12 d-flex flex-wrap'>
              {departments.map((item, index) => (
                <div key={index} className='mr-3'>
                  <CustomCheckbox>{item.label}</CustomCheckbox>
                </div>
              ))}
            </div>
          </div>
          <div className='mb-5'>
            <ButtonRed>{t('Найти вакансии')}</ButtonRed>
          </div>
        </div>
      </section>

      <div className='container'>
        <div className='d-flex fz-18'>
          Найдено вакансий: <div className='font-weight-bold ml-1'> 100</div>
        </div>
        <div className='row my-5'>
          {vacanciesList.map((item, index) => (
            <div key={index} className='col-12 mb-5'>
              <VacancyCard {...item} t={t} />
            </div>
          ))}
        </div>
        <div className='mb-5'>
          <ButtonRed>{t('Посмотреть еще')}</ButtonRed>
        </div>
      </div>
      <div className='container'>
        <div className='row'>
          {vacancyCategories.map((item, index) => (
            <div className='col-md-4 col-sm-6 col-12' key={index}>
              <div className='mb-3'>{item.title}</div>
              <ul>
                {item.list.map((element, ind) => (
                  <li key={ind}>{element.name}</li>
                ))}
              </ul>
            </div>
          ))}
        </div>
      </div>
      <section className='section vacancy-interests'>
        <Card className='no-vacancy text-center'>
          <h2 className='title text-uppercase text-dark'>
            НЕ НАШЛИ ПОДХОДЯЩУЮ ВАКАНСИЮ?
          </h2>
          <p className='text'>
            Пришлите свое резюме, рассмотрим, когда появится <br />
            подходящее предложение, свяжемся
          </p>
          <div>
            <ProductModal buttonLabel='Отправить резюме'>
              <ModalContent t={t} />
            </ProductModal>
          </div>
        </Card>
        <div className='container'>
          <div className='row'>
            <div className='col-12 col-md-7 dark-bg'>
              <h2 className='title'>
                РАБОТАЯ С НАМИ, ВАС ЖДЕТ <br />
                МНОГО ИНТЕРЕСНОГО
              </h2>
              <div className='text-white mt-3 d-block'>
                Комфортные условия работы в любой из 14 стран представительства
              </div>
              <ul className='list mt-5 text-white'>
                <li className='list-item mb-5'>
                  Через год вы будете успешным человеком <br />
                  твердно стоящим на ногах
                </li>
                <li className='list-item mb-5'>
                  Официальное трудоустройство с зарплатой выше <br />
                  рынка в компании работающей в 14 странах
                </li>
                <li className='list-item mb-5'>
                  Постоянный карьерный рост с возможностью <br />
                  переезда в другую страну
                </li>
                <li className='list-item'>
                  За свой счет обучаем и проводим от <br />
                  новичка до эксперта в своей области
                </li>
              </ul>
              <div className='interests-bottom d-flex align-items-center w-100 text-white'>
                <div>
                  <img
                    src={youtubeButton}
                    alt=''
                    className='img-responsive mr-3'
                  />
                  Смотреть видео
                </div>
                <div className='ml-5'>
                  <img
                    width='40px'
                    className='img-responsive mr-3'
                    src={photoCamera}
                    alt=''
                  />{' '}
                  Смотреть фото
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <section className='section pt-4'>
        <div className='container'>
          <div className='row'>
            <div className='col-12 col-md-5 order-md-1'>
              <div className='dashed-red text-center'>
                {t('Работая с нами, вы получаете больше, чем просто работа')}
              </div>
            </div>
            <div className='col-12 col-md-7 order-md-0'>
              <h2 className='vacancy__title mt-5'>{t('СМОТРИТЕ ИСТОРИИ')}</h2>
              <p className='mt-3'>
                {t(
                  'пришедших работать к нам и имеющие сейчас колоссальные результаты'
                )}
              </p>
              <p className='text-secondary mt-4 mb-0'>
                {t('Вы можете стать одним из них добившись своего успеха')}
              </p>
            </div>
          </div>
          <div className='black-slider mt-3 pb-4'>
            <Carousel
              additionalTransform={0}
              ssr
              infinite={true}
              arrows={true}
              draggable={false}
              responsive={sliderOptions}
              minimumTouchDrag={false}
              focusOnSelect={false}
              sliderClass='pb-5'
            >
              {staffGallery.map((item, idx) => (
                <div className='slider-item text-center' key={idx}>
                  <div className='slider-item__preview'>
                    <img src={item.image} alt='' />
                    <strong className='d-block mt-3'>
                      {t('Иванова Мария')}
                    </strong>
                    <p className='mb-0'>{t('должность')}</p>
                  </div>
                </div>
              ))}
            </Carousel>
          </div>
        </div>
      </section>
      <section className='section mt-5 map-wrap'>
        <div className='container'>
          <div className='map-text'>
            {t('РАБОТАЕМ В 14 СТРАНАХ И 20 ГОРОДАХ')}
          </div>
        </div>
      </section>
      <section className='section mt-5'>
        <div className='container'>
          <h2 className='title mb-5 font-weight-bold'>
            ПРО НАС ГОВОРЯТ И ПИШУТ <br />
            ПО ВСЕМУ МИРУ
          </h2>
          <div className='row'>
            <div className='col-12 col-md-6 black-slider'>
              <Carousel
                additionalTransform={0}
                ssr
                infinite={true}
                arrows={true}
                draggable={false}
                responsive={sliderOptionsOne}
                minimumTouchDrag={false}
                focusOnSelect={false}
                sliderClass='pb-5'
              >
                {staffGallery.map((item, idx) => (
                  <div className='slider-item' key={idx}>
                    <div className='slider-item__preview'>
                      <p>
                        <span className='font-weight-bold'>
                          {t('Иванова Мария,')}
                        </span>{' '}
                        {t('компания IT brand, Украина')}
                      </p>
                      <img src={item.image} alt='' className='img-responsive' />
                    </div>
                  </div>
                ))}
              </Carousel>
            </div>
            <div className='col-12 col-md-6 black-slider'>
              <Carousel
                additionalTransform={0}
                ssr
                infinite={true}
                arrows={true}
                draggable={false}
                responsive={sliderOptionsOne}
                minimumTouchDrag={false}
                focusOnSelect={false}
                sliderClass='pb-5'
              >
                {magazineGallery.map((item, idx) => (
                  <div className='slider-item' key={idx}>
                    <div className='slider-item__preview'>
                      <p className='font-weight-bold'>{t(item.title)}</p>
                      <img src={item.image} alt='' className='img-responsive' />
                    </div>
                  </div>
                ))}
              </Carousel>
            </div>
          </div>
        </div>
      </section>
    </Layout>
  )
}

const ModalContent = ({ t }) => {
  return (
    <div className='vacancy-modal'>
      <div className='title'>{t('Пришлите свое резюме')}</div>
      <div className='text mt-3 mb-4'>
        {t('Рассмотрим, когда появится подходящее предложение, свяжемся')}
      </div>
      <FormVacancy t={t} mode='modal' />
    </div>
  )
}

VacancyAll.getInitialProps = async () => ({
  namespacesRequired: ['common'],
})

export default withTranslation('common')(VacancyAll)
