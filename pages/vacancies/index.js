import React from 'react'
import { Layout } from 'components/layout'
import { withTranslation } from 'utils/with-i18next'
import Breadcrumbs from 'components/breadcrumbs'
import Carousel from 'react-multi-carousel'
import {
  responsiveCard,
  staffGallery,
  countries,
  departments,
} from 'utils/data'
import { ButtonRed, Checkbox, CustomCheckbox } from 'components/form'
import { Button } from 'reactstrap'

import advantage1 from 'public/images/vacancies/icons/advantage-1.png'
import advantage2 from 'public/images/vacancies/icons/advantage-2.png'
import advantage3 from 'public/images/vacancies/icons/advantage-3.png'

const Vacancies = ({ t }) => {
  const sliderOptions = responsiveCard({ desktopItems: 2 })
  return (
    <Layout>
      <section className='vacancy text-white pb-5'>
        <div className='container'>
          <h2 className='vacancy__title mb-3'>
            <span className='mb-3 d-block'>
              {t('Перестаньте тратить жизнь на нелюбимую работу')}
            </span>
            {t('Работайте в компании с мировой известностью')}
          </h2>
          <h1 className='vacancy__title text-uppercase pb-5'>
            {t('IT ДИСТРИБЬЮТОР MUK')}
          </h1>
          <ul className='d-flex advantage-list flex-column flex-sm-row'>
            <li className='advantage-item d-flex d-sm-inline-block'>
              <img src={advantage1} alt='' className='img-responsive' />
              <div className='mt-3'>{t(`Зарплата выше средней по рынку`)}</div>
            </li>
            <li className='advantage-item d-flex d-sm-inline-block'>
              <img src={advantage2} alt='' className='img-responsive' />
              <div className='mt-3'>{t('Официальное трудоустройство')}</div>
            </li>
            <li className='advantage-item d-flex d-sm-inline-block'>
              <img src={advantage3} alt='' className='img-responsive' />
              <div className='mt-3'>{t('Обучаем и взращиваем экспертов')}</div>
            </li>
          </ul>
        </div>
      </section>
      <section className='section pt-4'>
        <div className='container'>
          <Breadcrumbs data={[{ name: 'Вакансии' }]} mode='white' />
          <div className='row'>
            <div className='col-12 col-md-7'>
              <h2 className='vacancy__title'>
                {t(
                  'СМОТРИТЕ КАК ПРОВОДЯТ ВРЕМЯ НАШИ СОТРУДНИКИ В РАБОЧЕЕ И НЕ РАБОЧЕЕ ВРЕМЯ'
                )}
              </h2>
            </div>
          </div>
          <div className='black-slider mt-5'>
            <Carousel
              additionalTransform={0}
              ssr
              infinite={true}
              arrows={true}
              draggable={false}
              responsive={sliderOptions}
              minimumTouchDrag={false}
              focusOnSelect={false}
              sliderClass='pb-5'
            >
              {staffGallery.map((item, idx) => (
                <div className='slider-item' key={idx}>
                  <div className='slider-item__preview'>
                    <div className='slider-item__title'>{item.title}</div>
                    <img src={item.image} alt='' />
                  </div>
                </div>
              ))}
            </Carousel>
          </div>
        </div>
      </section>
      <div className='container mt-5'>
        <h2 className='vacancy__title'>{t('НАЙДИТЕ ВАКАНСИЮ МЕЧТЫ')}</h2>
        <div className='row mt-3 mb-3'>
          <div className='col-md-2 col-6 d-flex mt-3 mb-3'>
            <Checkbox>Без опыта</Checkbox>
          </div>
          <div className='col-md-2 col-6 d-flex  mt-3 mb-3'>
            <Checkbox>Удаленная работа</Checkbox>
          </div>
          <div className='col-md-2 col-12 d-flex mt-3 mb-3'>
            <Checkbox>Возможность переезда</Checkbox>
          </div>
        </div>
        <div className='row mt-4 mb-5 flex-wrap'>
          <div className='col-12 mb-4'>{t('Выберите страну')}</div>
          <div className='col-md-10 col-12 d-flex flex-wrap'>
            {countries.map((item, index) => (
              <div key={index} className='mr-3'>
                <CustomCheckbox>{item.label}</CustomCheckbox>
              </div>
            ))}
          </div>
        </div>
        <div className='row mt-5 mb-5'>
          <div className='col-12 mb-4'>{t('Выберите страну')}</div>
          <div className='col-md-10 col-12 d-flex flex-wrap'>
            {departments.map((item, index) => (
              <div key={index} className='mr-3'>
                <CustomCheckbox>{item.label}</CustomCheckbox>
              </div>
            ))}
          </div>
        </div>
        <div className='mb-5'>
          <ButtonRed>{t('Найти вакансии')}</ButtonRed>
          <br />
          <Button color='link' className='p-0 mt-5 text-muted'>
            {t('Вернутся на страницу вакансий')}
          </Button>
        </div>
      </div>
    </Layout>
  )
}

Vacancies.getInitialProps = async () => ({
  namespacesRequired: ['common'],
})

export default withTranslation('common')(Vacancies)
