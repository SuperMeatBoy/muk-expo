import nextConnect from 'next-connect'
import middleware from 'middleware'
import { ObjectID } from 'mongodb'

const handler = nextConnect()

handler
  .use(middleware)
  .get((req, res) => {
    // prettier-ignore
    const dataModel = { "_id": new ObjectID(), "hello": 'world', }
    res.status(200).json(dataModel)
  })
  .post((req, res) => {
    res.json({ hello: 'world' })
  })
  .put(async (req, res) => {
    res.end('async/await is also supported!')
  })

export default handler
