import React from 'react'
import App from 'next/app'
import { appWithTranslation } from 'utils/with-i18next'

import 'style/index.scss'

class MyApp extends App {
  render() {
    const { Component, pageProps } = this.props
    return <Component {...pageProps} />
  }
}

export default appWithTranslation(MyApp)
