import React from 'react'
import { Layout } from 'components/layout'
import { Link, withTranslation } from 'utils/with-i18next'
import CoursePrice from 'components/course-price'
import { SearchBlock } from 'components/form'
import ReviewSlider from 'components/review-slider'
import { Tab, TabList, TabPanel, Tabs } from 'react-tabs'
import { serviceBrandList, servicePriceList, tariffs } from 'utils/data'
import Icon from 'components/icons'
import Certificate from 'public/images/service/certificat.jpg'
import BrandSlider from 'components/brand-slider'
import ThisGivesPreview from 'public/images/service/advantage.jpg'
import FormConsultation from 'components/form-consultation'

import ico1 from 'public/images/service/ico-computer-tree.png'
import ico2 from 'public/images/service/ico-podpis.png'
import ico3 from 'public/images/service/ico-check-setting.png'

import ico4 from 'public/images/service/ico-graph.png'
import ico5 from 'public/images/service/ico-money.png'
import ico6 from 'public/images/service/ico-medal.png'
import MasterSlider from '../components/master-slider'

import dynamic from 'next/dynamic'
const Map = dynamic(() => import('components/map'), {
  ssr: false,
})

const course = {
  title: 'АВТОРИЗИРОВАННЫЙ СЕРВИСНЫЙ ЦЕНТР КОМПЬЮТЕРНОЙ ТЕХНИКИ МИРОВЫХ БРЕНДОВ',
  description: 'Ремонтируйте технику с лучшими',
  list: [
    {
      icon: 'serviceComputer',
      title: 'Работаем с частными клиентами и компанями',
    },
    {
      icon: 'medal',
      title: 'Система менеджмента качества ISO 9001',
    },
    {
      icon: 'computer',
      title: 'Оригинальные запасные части',
    },
  ],
}

const Service = (props) => {
  const { t, apiKey } = props
  return (
    <Layout header='white'>
      <section className='section section__service-one'>
        <div className='container'>
          <CoursePrice service small course={course} t={t}/>
        </div>
      </section>
      <section className='section section__service-two'>
        <div className="container">
          <h2 className='title title__section fz-30'>{t('СДЕЛАЙТЕ РЕМОНТ ЛЮБОЙ ТЕХНИКИ В СРОК ОТ 1 ДНЯ')}</h2>
          <h5 className='title title__description'>{t('Выберите, что нужно ремонтировать и смотрите цену')}</h5>
          <SearchBlock/>
          <div className='tabs-service'>
            <Tabs>
              <TabList>
                <Tab>Ремонт ноутбуков</Tab>
                <Tab>Ремонт телефонов</Tab>
                <Tab>Ремонт планшетов</Tab>
                <Tab>Ремонт ПК</Tab>
              </TabList>
              <TabPanel>
                <table className='service-price-list'>
                  <thead>
                  <tr>
                    <th>{t('Наименование работ')}</th>
                    <th>{t('Цена, грн.')}</th>
                    <th></th>
                  </tr>
                  </thead>
                  <tbody>
                  {servicePriceList.map(service => (
                    <tr key={service.id}>
                      <td>{service.name}</td>
                      <td>{service.ot && t('от')}{service.price}</td>
                      <td>
                        <button>{t('Заказать')}</button>
                      </td>
                    </tr>
                  ))}
                  </tbody>
                </table>
              </TabPanel>
              <TabPanel></TabPanel>
              <TabPanel></TabPanel>
              <TabPanel></TabPanel>
            </Tabs>
          </div>
        </div>
      </section>
      <section className='section section__service-three'>
        <div className="container">
          <h2 className='title title__section title__section--white fz-30'>{t('ВЫПОЛНЯЕМ РЕМОНТ ТЕХНИКИ ПО ГАРАНТИИ')}</h2>
          <div className='flex'>
            <div className='remont-certificates-titles'>
              <h5
                className='title title__description title__description--white'>{t('Hp, Hitachi Vantara, Fujitsu, Ibm, Dell Emc, Fortinet, Grandstream, Tripp Lite, Oracle, Apc, Cisco, Extreme, Checkpoint, Enot При обращении в сервисный центр при себе необходимо иметь')}</h5>
              <h4 className='title title__description title__description--white'>{t('Гарантийный талон')}</h4>
              <h5
                className='title title__description title__description--white'>{t('Документы подтверждающие покупку (чек, счет, накладная)')}</h5>
            </div>
            <div className="remont-certificates flex">
              <div>
                <img src={Certificate} alt=""/>
                <div className="enlarge">
                  {t('Увеличить')}
                  <Icon component='magnifier'/>
                </div>
              </div>
              <div>
                <img src={Certificate} alt=""/>
                <div className="enlarge">
                  {t('Увеличить')}
                  <Icon component='magnifier'/>
                </div>
              </div>
              <div>
                <img src={Certificate} alt=""/>
                <div className="enlarge">
                  {t('Увеличить')}
                  <Icon component='magnifier'/>
                </div>
              </div>
            </div>
          </div>
          <div className='service-brand-list-title'>
            {t('Проверить устройство на гарантию на официальных сайтах производителей')}
          </div>
          <div className="service-brand-list flex">
            {serviceBrandList.map(brand => (
              <div key={brand.id}>
                <div className="service-brand">
                  <div className="service-brand__preview">
                    <img src={brand.image} alt=""/>
                  </div>
                  <a href={brand.url} target='_blank'>{t('Проверить')}</a>
                </div>
              </div>
            ))}
          </div>
        </div>
      </section>
      <section className='section section__service-four'>
        <div className="container">
          <h2 className='title title__section fz-36'>{t('ОФИЦИАЛЬНО РЕМОНТИРУЕМ ТЕХНИКУ МИРОВЫХ БРЕНДОВ ПО СТАНДАРТКУ КАЧЕСТВА ISO 9001')}</h2>
          <div className='black-slider'>
            <BrandSlider />
          </div>
          <div className="this-gives">
            <div className="title title__section title__this-gives">{t('Это дает')}:</div>
            <div className="flex this-gives__advantage">
              <div className="this-gives__preview">
                <img src={ThisGivesPreview} alt=""/>
              </div>
              <div className="this-gives__list">
                <div>{t('Время ремонта от 2 часов')}</div>
                <div>{t('Замена только на оригинальные запчасти')}</div>
                <div>{t('Бережное отношение к технике')}</div>
                <div>{t('Гарантию на ремонт от 6 месяцев')}</div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <section className='section section__service-five'>
        <div className="container">
          <h2 className='title title__section fz-30'>{t('ПУСТЬ ВАШ БИЗНЕС РАБОТАЕТ БЕЗ ПРОСТАИВАНИЯ')}</h2>
          <h5 className='title title__description'>{t('Услуга контрактного сервисного обслуживания и восстановления работоспособности оборудования компании')}</h5>
          <div className="this-gives">
            <div className="flex this-gives__advantage">
              <div className="this-gives__preview">
                <img src={ThisGivesPreview} alt=""/>
              </div>
              <div className="this-gives__body">
                <p>
                  {t('Заключаем договор на 1 год и вся техника вашей компании будет под круглосуточным контролем')}
                </p>
                <p>
                  {t('При поломке мы выезжаем на ремонт в')}
                   <strong> {t('любое время дня и ночи')} </strong>
                </p>
                <p>
                  {t('Сертифицированные специалисты не допустят')}
                  <strong> {t('потерю критически важных данных')} </strong>
                  {t('с жестких дисков')}
                </p>
                <p>
                  {t('Работают по сертификату ISO 9001 Бюро Международной Сертификации')}
                </p>
                <p>
                  {t('Более 1000')}
                  <strong> {t(' оригинальных запчастей')} </strong>
                  {t('на собственном складе готовых к ремонту')}
                </p>
              </div>
              <div className="this-gives__video">
                <div className="this-gives__video-preview">
                  <video src="" className='video'></video>
                </div>
                <div className="flex this-gives__video-btn-group">
                  <a href="#" download className='this-gives__video-btn-download'>
                    {t('Скачать пример договора')}
                  </a>
                  <Link href='/'>
                    <a className='this-gives__video-btn-preview'>{t('Посмотреть отзывы клиентов')}</a>
                  </Link>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <section className='section section__service-six'>
        <div className="container">
          <h2 className='title title__section fz-30'>{t('ТАРИФЫ НА КОНТРАКТНОЕ ОБСЛУЖИВАНИЕ')}</h2>
          <div className="row">
            {tariffs.map((tariff, idx) => (
              <div className="col-sm-6 col-md-6 col-lg-3 mb-5" key={idx}>
                <div className={`tariff ${tariff.hit && 'hit'}`} data-text={tariff.hit ? t('Хит продаж') : 'none'}>
                  <div className="tariff__title">{tariff.title}</div>
                  <div className="tariff__region">{t('Регион работы')}: <span>{tariff.region}</span></div>
                  <div className="tariff__title-des">{t('Сроки устранения неполадок и ремонта')}</div>
                  <div className="tariff__des">{tariff.description}</div>
                  <div className="tariff__price">{tariff.price} {t('грн./мес.')}</div>
                  <div className="tariff__add">{t('Получить консультацию')}</div>
                </div>
              </div>
            ))}
          </div>
        </div>
      </section>
      <section className='section section__service-seven'>
        <div className="container">
          <h2 className='title title__section fz-30'>{t('ПРИГЛАШАЕМ ВЕНДОРОВ IT ТЕХНОЛОГИЙ ДЛЯ ВЗАИМОВЫГОДНОГО СОТРУДНИЧЕСТВА')}</h2>
          <div className="row">
            <div className="col-md-7">
              <h5 className="title title__description">{t('Увеличиваете продажи техники вашего бренда, за счет выхода на новые рынки сбыта в городах Украины')}</h5>
                <div className="standards-list">
                  <div>
                    <img src={ico1} alt=""/>
                    {t('Получаете полностью готовый центр к ремонту техники ваших клиентов по гарантии стандарта ISO 9001')}
                  </div>
                  <div>
                    <img src={ico2} alt=""/>
                    {t('Одним договором вы получаете официальные сервисные центры во всех регионах Украины')}
                  </div>
                  <div>
                    <img src={ico3} alt=""/>
                    {t('Люди охотнее покупают технику вашего бренда, когда в их городе есть официальный сервисный центр')}
                  </div>
                </div>
            </div>
            <div className="col-md-5">
              <FormConsultation email />
            </div>
          </div>
        </div>
      </section>
      <section className='section section__service-eight'>
        <div className="container">
          <h2 className='title title__section fz-36'>{t('ВЫ МОЖЕТЕ СТАТЬ ПАРТНЕРОМ НАШЕГО СЕРВИСА В СВОЕМ РЕГИОНЕ УКРАИНЫ')}</h2>
          <div className="box-my-partner flex">
            <FormConsultation />
            <div className="box-my-partner-list">
              <div>Получите</div>
              <div>
                <img src={ico4} alt=""/>
                {t('Постоянный поток клиентов и рост прибыли')}
              </div>
              <div>
                <img src={ico5} alt=""/>
                {t('Сможете переживать любой кризис без потерь')}
              </div>
              <div>
                <img src={ico6} alt=""/>
                {t('О вашей компании будет знать весь ваш город')}
              </div>
            </div>
          </div>
        </div>
      </section>
      <ReviewSlider t={t} title='ОТЗЫВЫ ОТ КОМПАНИЙ И ЧАСТНЫХ КЛИЕНТОВ'/>
      <section className='section section__service-ten'>
        <div className="container">
          <h2 className='title title__section fz-30'>{t('ДОВЕРЬТЕ РЕМОНТ ТЕХНИКИ СЕРТИФИЦИРОВАННЫМ МАСТЕРАМ С ОПЫТОМ ОТ 7 ДО 25 ЛЕТ')}</h2>
          <h5 className='title title__description'>{t('Работают по сертификату ISO 9001 Бюро Международной Сертификации')}</h5>
          <MasterSlider t={t} />
        </div>
      </section>
      <section className='section section__service-eleven'>
        <div className="container">
          <h2 className='title title__section title__section--white fz-30'>{t('КОНТАКТЫ СЕРВИСНЫХ ЦЕНТРОВ В КИЕВЕ')}</h2>
          <div className='tabs-contacts'>
            <Tabs>
              <TabList>
                <Tab>Основной центр</Tab>
                <Tab>Пункт приемки</Tab>
              </TabList>
              <TabPanel>
                <div className="tab-contact flex">
                  <div>
                    <div className="contact-box">
                      <div className="contact-box__title">{t('Контакты')}</div>
                      <div className="contact-box__des">{t('Телефон')}: (044) 492-29-09, 594-97-97 ({t('многоканальные')})</div>
                      <div className="contact-box__des">E-mail: service@muk.ua</div>
                    </div>
                    <div className="contact-box">
                      <div className="contact-box__title">{t('Время работы')}</div>
                      <div className="contact-box__des">{t('Пн.-Пт.: 09:00-18:00')},</div>
                      <div className="contact-box__des">{t('Сб.-Вс. выходные')}</div>
                    </div>
                    <div className="contact-box">
                      <div className="contact-box__title">{t('Адрес')}</div>
                      <div className="contact-box__des"> {t('03151, г. Киев, ул. Смелянская 17а (район Караваевых дач)')}</div>
                    </div>
                    <div className="contact-box">
                      <div className="contact-box__title">{t('Как проехать')}</div>
                      <div className="contact-box__des"> {t('Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar sic tempor. Sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nam fermentum, nulla luctus pharetra vulputate, felis tellus mollis orci, sed rhoncus pronin sapien nunc accuan eget')}</div>
                    </div>
                  </div>
                  <div>
                    <div className="karta">
                      <Map
                        apiKey={apiKey}
                        initialCenter={{ lat: '41.7323738', lng: '44.698428' }}
                        zoom={12}
                      />
                    </div>
                    <div className="karta-btn-group flex">
                      <div className="karta-btn flex">
                        <Icon component='picture' />
                        Смотрите фото сервиса
                      </div>
                      <a href='https://youtube.com/' className="karta-btn flex">
                        <Icon component='youtube' />
                        Онлайн web камера
                      </a>
                    </div>
                  </div>
                </div>
              </TabPanel>
              <TabPanel>
                <div className="tab-contact flex">
                  <div>
                    <div className="contact-box">
                      <div className="contact-box__title">{t('Контакты')}</div>
                      <div className="contact-box__des">{t('Телефон')}: (044) 492-29-09, 594-97-97 ({t('многоканальные')})</div>
                      <div className="contact-box__des">E-mail: service@muk.ua</div>
                    </div>
                    <div className="contact-box">
                      <div className="contact-box__title">{t('Время работы')}</div>
                      <div className="contact-box__des">{t('Пн.-Пт.: 09:00-18:00')},</div>
                      <div className="contact-box__des">{t('Сб.-Вс. выходные')}</div>
                    </div>
                    <div className="contact-box">
                      <div className="contact-box__title">{t('Адрес')}</div>
                      <div className="contact-box__des"> {t('03151, г. Киев, ул. Смелянская 17а (район Караваевых дач)')}</div>
                    </div>
                    <div className="contact-box">
                      <div className="contact-box__title">{t('Как проехать')}</div>
                      <div className="contact-box__des"> {t('Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar sic tempor. Sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nam fermentum, nulla luctus pharetra vulputate, felis tellus mollis orci, sed rhoncus pronin sapien nunc accuan eget')}</div>
                    </div>
                  </div>
                  <div>
                    <div className="karta">
                      <Map
                        apiKey={apiKey}
                        initialCenter={{ lat: '21.7323738', lng: '55.698428' }}
                        zoom={12}
                      />
                    </div>
                    <div className="karta-btn-group flex">
                      <div className="karta-btn flex">
                        <Icon component='picture' />
                        Смотрите фото сервиса
                      </div>
                      <a href='https://youtube.com/' className="karta-btn flex">
                        <Icon component='youtube' />
                        Онлайн web камера
                      </a>
                    </div>
                  </div>
                </div>
              </TabPanel>
            </Tabs>
          </div>
        </div>
      </section>
    </Layout>
  )
}

Service.getInitialProps = async () => ({
  namespacesRequired: ['common'],
  apiKey: process.env.GOOGLE_API_KEY,
})

export default withTranslation('common')(Service)
