import React from 'react'
import { Layout } from 'components/layout'
import { withTranslation } from 'utils/with-i18next'
import Breadcrumbs from 'components/breadcrumbs'
import { courseList } from 'utils/data'

import { CourseCard } from 'components/card'
import { CouponForm } from 'components/form'
import FormITConsultant from 'components/form/itConsultant'

import advantage1 from 'public/images/training/icons/advantage-1.png'
import advantage2 from 'public/images/training/icons/advantage-2.png'
import advantage3 from 'public/images/training/icons/advantage-3.png'

import businessmans from 'public/images/training/businessmans.jpg'
import corporate from 'public/images/training/corporate.jpg'

const TrainingCategory = (props) => {
  const { t } = props

  return (
    <Layout>
      <section className='big-dark training text-white pb-5'>
        <div className='container'>
          <h1 className='title text-uppercase m-auto pb-4'>
            {t('КУРСЫ ПО AUTOCAD ОТ ЭКСПЕРТОВ УЧЕБНОГО ЦЕНТРА MUK')}
          </h1>
          <h2 className='vacancy__title mb-5'>
            <span className='d-block'>
              {t('Будьте всегда на волне новых знаний')}
            </span>
          </h2>
          <ul className='d-flex advantage-list flex-column flex-sm-row pt-5'>
            <li className='advantage-item d-flex d-sm-inline-block'>
              <img src={advantage1} alt='' className='img-responsive' />
              <div className='mt-3'>
                {t(`Удобный формат оффлайн и онлайн обучения`)}
              </div>
            </li>
            <li className='advantage-item d-flex d-sm-inline-block'>
              <img src={advantage2} alt='' className='img-responsive' />
              <div className='mt-3'>
                {t('Получение международного сертификата')}
              </div>
            </li>
            <li className='advantage-item d-flex d-sm-inline-block'>
              <img src={advantage3} alt='' className='img-responsive' />
              <div className='mt-3'>{t('Курсы с учетом реалий 2020 года')}</div>
            </li>
          </ul>
        </div>
      </section>
      <section className='section pt-4'>
        <div className='container'>
          <Breadcrumbs data={[{ name: 'Курсы по Autocad' }]} mode='white' />
          <div className='row'>
            <div className='col-md-6 mb-4'>
              <img className='img-responsive' src={businessmans} alt='' />
            </div>
            <div className='col-md-6 mb-4'>
              <img className='img-responsive' src={corporate} alt='' />
            </div>
          </div>
        </div>
      </section>

      <section className='section mt-5 pb-5'>
        <div className='container'>
          <h3 className='font-weight-bold'>
            {t('Подберите курс под свои потребности')}
          </h3>
          <div className='row mt-5'>
            {courseList.map((item, index) => (
              <div className='col-md-6' key={index}>
                <CourseCard {...item} />
              </div>
            ))}
          </div>
        </div>
      </section>

      <section className='section section__for-from-course mt-5 text-white'>
        <div className='container'>
          <h3 className='title text-white h3 font-weight-bold'>
            {t('Часто покупают вместе')}
          </h3>
        </div>
      </section>

      <div className='container mt-5'>
        <div className='row'>
          <FormITConsultant t={t} mode='training' />
        </div>
      </div>

      <section className='section mt-5'>
        <CouponForm t={t} />
      </section>
    </Layout>
  )
}

TrainingCategory.getInitialProps = async () => ({
  namespacesRequired: ['common'],
})

export default withTranslation('common')(TrainingCategory)
