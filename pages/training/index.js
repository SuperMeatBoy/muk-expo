import React, { useState } from 'react'
import { Layout } from 'components/layout'
import { withTranslation } from 'utils/with-i18next'
import Breadcrumbs from 'components/breadcrumbs'
import {
  brandsList,
  responsiveCard,
  courseList,
  categoryCourseList,
  newsItMarketStudy,
} from 'utils/data'

import {
  TabContent,
  TabPane,
  Nav,
  NavItem,
  NavLink,
  Card,
  Button,
  CardTitle,
  CardText,
  Row,
  Col,
} from 'reactstrap'
import Carousel from 'react-multi-carousel'
import { CourseCard } from 'components/card'
import { ButtonRed, SearchBlock, CouponForm } from 'components/form'
import FormITConsultant from 'components/form/itConsultant'
import { Calendar } from 'components/calendar'
import clsx from 'clsx'

import dynamic from 'next/dynamic'
const Map = dynamic(() => import('components/map'), {
  ssr: false,
})

import advantage1 from 'public/images/training/icons/advantage-1.png'
import advantage2 from 'public/images/training/icons/advantage-2.png'
import advantage3 from 'public/images/training/icons/advantage-3.png'

import businessmans from 'public/images/training/businessmans.jpg'
import corporate from 'public/images/training/corporate.jpg'
import standard1 from 'public/images/training/brands/pearson.png'
import standard2 from 'public/images/training/brands/red-hat.png'
import BrandSlider from '../../components/brand-slider'

const TrainingHome = (props) => {
  const { t, apiKey } = props
  const [activeTab, setActiveTab] = useState('1')

  const toggle = (tab) => activeTab !== tab && setActiveTab(tab)

  return (
    <Layout>
      <section className='big-dark training text-white pb-5'>
        <div className='container'>
          <h1 className='title text-uppercase m-auto pb-4'>
            {t('КУРСЫ ПО AUTOCAD ОТ ЭКСПЕРТОВ УЧЕБНОГО ЦЕНТРА MUK')}
          </h1>
          <h2 className='vacancy__title mb-5'>
            <span className='d-block'>
              {t('Будьте всегда на волне новых знаний')}
            </span>
          </h2>
          <ul className='d-flex advantage-list flex-column flex-sm-row pt-5'>
            <li className='advantage-item d-flex d-sm-inline-block'>
              <img src={advantage1} alt='' className='img-responsive' />
              <div className='mt-3'>
                {t(`Удобный формат оффлайн и онлайн обучения`)}
              </div>
            </li>
            <li className='advantage-item d-flex d-sm-inline-block'>
              <img src={advantage2} alt='' className='img-responsive' />
              <div className='mt-3'>
                {t('Получение международного сертификата')}
              </div>
            </li>
            <li className='advantage-item d-flex d-sm-inline-block'>
              <img src={advantage3} alt='' className='img-responsive' />
              <div className='mt-3'>{t('Курсы с учетом реалий 2020 года')}</div>
            </li>
          </ul>
        </div>
      </section>
      <section className='section pt-4'>
        <div className='container'>
          <Breadcrumbs data={[{ name: 'Курсы MUK' }]} mode='white' />
          <div className='row'>
            <div className='col-md-6 mb-4'>
              <img className='img-responsive' src={businessmans} alt='' />
            </div>
            <div className='col-md-6 mb-4'>
              <img className='img-responsive' src={corporate} alt='' />
            </div>
          </div>
          <div className='subtitle mb-4'>
            ИМЕЕМ АВТОРИЗАЦИЮ ОТ 20 МИРОВЫХ БРЕНДОВ
          </div>
          <div className='row'>
            <div className='col-md-12 black-slider'>
              <BrandSlider />
            </div>
          </div>
        </div>
      </section>
      <section className='section section__for-from-course mt-5 text-white'>
        <div className='container'>
          <div className='row'>
            <div className='col-md-7'>
              <h2 className='title title__section text-white fz-30'>
                {t(
                  'ОТПРАВЛЯЙТЕ СВОИХ СОТРУДНИКОВ НА СЕРТИФИКАЦИЮ ПО МЕЖДУНАРОДНЫМ СТАНДАРТАМ'
                )}
              </h2>
              <p>
                {t(
                  'и получайте сотрудников с новыми знаниями, которые выведут вашу компанию на новый уровень'
                )}
              </p>
              <ul>
                <li>
                  Свой авторизованный центр тестирования и индивидуальной
                  станции Red hat
                </li>
                <li>
                  Позволит пройти авторизованное обучение и получить сертификат,
                  подтверждающий полученные знания и признанный во всем мире
                </li>
                <li>
                  Принимаем сертификационные экзамены практически всех доступных
                  вендоров
                </li>
              </ul>
              <Button color='link' className='p-0 mb-4'>
                Как проходит сертификация
              </Button>
              <p>
                Получите более подробную информацию о сертификации <br />
                отправив заявку на почту{' '}
                <a href='mailto:nla@muk.ua'>nla@muk.ua</a> или
                <a href='mailto:training@muk.ua'>training@muk.ua</a> для Яцко
                Наталии <br />
                позвонив на телефон{' '}
                <a href='tel:+38 044 492 29229'>+38 044 492 29229</a>, внут.
                492. <br />
                или оставив заявку в форме
              </p>
            </div>
            <div className='col-md-5'>
              <img src={standard1} alt='' className='img-responsive' />
              <img src={standard2} alt='' className='img-responsive' />
            </div>
          </div>
        </div>
      </section>
      <section className='section mt-5 pb-5'>
        <div className='container'>
          <h3 className='font-weight-bold'>Горячие курсы</h3>
          <div className='subtitle'>
            Выбирайте курсы со скидкой до 30%, обучение начинается в ближайшее
            время
          </div>
          <div className='row mt-5'>
            {courseList.map((item, index) => (
              <div className='col-md-6 col-12' key={index}>
                <CourseCard {...item} />
              </div>
            ))}
          </div>
          <h3 className='font-weight-bold mt-5'>Новые курсы / Хиты продаж</h3>
          <div className='row mt-5'>
            {courseList.map((item, index) => (
              <div className='col-md-6 col-12' key={index}>
                <CourseCard {...item} />
              </div>
            ))}
          </div>
          <div className='text-center mt-3'>
            <ButtonRed>{t('Показать еще')}</ButtonRed>
          </div>
        </div>
      </section>

      <section className='section mt-5'>
        <div className='container'>
          <h3 className='font-weight-bold mb-3'>
            Подберите курс под свои потребности
          </h3>
          <SearchBlock />
          <div className='row mt-3'>
            {categoryCourseList.map((item, index) => (
              <div className='col-md-4' key={index}>
                <div className='d-flex category-course-card my-4'>
                  <div className='img-wrapper'>
                    <img
                      src={item.image}
                      alt=''
                      width='130'
                      className='img-responsive'
                    />
                  </div>
                  <div className='text-wrapper ml-3'>
                    <div className='font-weight-bold'>{item.name}</div>
                    <ul className='mt-3'>
                      {item.list.map((item, index) => (
                        <li key={index} className='mb-1'>
                          {item}
                        </li>
                      ))}
                    </ul>
                  </div>
                </div>
              </div>
            ))}
          </div>
        </div>
      </section>

      <section className='section mt-5 calendar-wrapper'>
        <div className='container py-5'>
          <h3 className='font-weight-bold'>РАСПИСАНИЕ КУРСОВ ПО МЕСЯЦАМ</h3>
          <p>Выбирайте день и месяц, справа смотрите какие курсы стартуют</p>
          <div>
            <Calendar />
          </div>
        </div>
      </section>

      <section className='section mt-5'>
        <div className='container'>
          <h3 className='font-weight-bold'>НОВОСТИ РЫНКА IT ОБРАЗОВАНИЯ</h3>
          <div className='row'>
            {newsItMarketStudy.map((item, index) => (
              <div className='col-md-4' key={index}>
                <Card body className='my-3'>
                  <div className='title font-weight-bold'>{item.name}</div>
                  <div className='date'>
                    {t('Дата')}: {item.date}
                  </div>
                  <div className='desctiption mt-2'>{item.description}</div>
                </Card>
              </div>
            ))}
          </div>
          <div className='text-center mt-4'>
            <ButtonRed>{t('Показать еще')}</ButtonRed>
          </div>
        </div>
      </section>
      <div className='container mt-5'>
        <div className='row'>
          <FormITConsultant t={t} mode='training' />
        </div>
      </div>

      <section className='section mt-5'>
        <CouponForm t={t} />
      </section>

      <section className='section section__for-from-course training-center-map mt-5 text-white'>
        <div className='container'>
          <h2 className='title title__section text-white fz-30'>
            {t('КОНТАКТЫ УЧЕБНЫХ ЦЕНТРОВ')}
          </h2>
          <div>
            <Nav tabs>
              <NavItem>
                <NavLink
                  className={clsx({ active: activeTab === '1' })}
                  onClick={() => toggle('1')}
                >
                  Украина
                </NavLink>
              </NavItem>
              <NavItem>
                <NavLink
                  className={clsx({ active: activeTab === '2' })}
                  onClick={() => toggle('2')}
                >
                  Азербайджан
                </NavLink>
              </NavItem>
              <NavItem>
                <NavLink
                  className={clsx({ active: activeTab === '3' })}
                  onClick={() => toggle('3')}
                >
                  Беларусь
                </NavLink>
              </NavItem>
              <NavItem>
                <NavLink
                  className={clsx({ active: activeTab === '4' })}
                  onClick={() => toggle('4')}
                >
                  Грузия
                </NavLink>
              </NavItem>
            </Nav>
            <TabContent activeTab={activeTab}>
              <TabPane tabId='1'>
                <Row className='my-5'>
                  <Col sm='5' className='mb-3'>
                    Марина Сокотунова / Maryna Sokotunova
                    <br />
                    Директор Учебного Центра/Training Center Director
                    <br />
                    e-mail: <a href='mailto:mso@muk.ua'>mso@muk.ua</a>
                    <br />
                    <a href='tel:+380444922929'>+38044 492-29-29</a>, ext. 491
                  </Col>
                  <Col sm='5' className='mb-3'>
                    Марина Сокотунова / Maryna Sokotunova
                    <br />
                    Директор Учебного Центра/Training Center Director
                    <br />
                    e-mail: <a href='mailto:mso@muk.ua'>mso@muk.ua</a>
                    <br />
                    <a href='tel:+380444922929'>+38044 492-29-29</a>, ext. 491
                  </Col>
                  <Col sm='5' className='mb-3'>
                    Марина Сокотунова / Maryna Sokotunova
                    <br />
                    Директор Учебного Центра/Training Center Director
                    <br />
                    e-mail: <a href='mailto:mso@muk.ua'>mso@muk.ua</a>
                    <br />
                    <a href='tel:+380444922929'>+38044 492-29-29</a>, ext. 491
                  </Col>
                  <Col sm='5' className='mb-3'>
                    Марина Сокотунова / Maryna Sokotunova
                    <br />
                    Директор Учебного Центра/Training Center Director
                    <br />
                    e-mail: <a href='mailto:mso@muk.ua'>mso@muk.ua</a>
                    <br />
                    <a href='tel:+380444922929'>+38044 492-29-29</a>, ext. 491
                  </Col>
                </Row>
                <Row>
                  <Col sm='4' className='mb-4'>
                    <div className='font-weight-bold mb-3'>
                      Украина, г. Киев, ул. Донецкая, 16/2 03151{' '}
                    </div>
                    телефон:{' '}
                    <a href='tel:+38 (044) 594-98-98'>+38 (044) 594-98-98</a>
                    <a href='tel:+38 (044) 492-29-29'>+38 (044) 492-29-29</a>
                    <br />
                    email: <a href='mailto:info@muk.ua'>info@muk.ua</a>
                  </Col>
                  <Col sm='8'>
                    <Map apiKey={apiKey} />
                  </Col>
                </Row>
              </TabPane>
              <TabPane tabId='2'>
                <Row>
                  <Col sm='12'>
                    <Map
                      apiKey={apiKey}
                      initialCenter={{ lat: '40.384839', lng: '49.839964' }}
                      zoom={15}
                    />
                  </Col>
                </Row>
              </TabPane>
              <TabPane tabId='3'>
                <Row>
                  <Col sm='12'>
                    <Map
                      apiKey={apiKey}
                      initialCenter={{ lat: '53.9055612', lng: '27.5459915' }}
                      zoom={15}
                    />
                  </Col>
                </Row>
              </TabPane>
              <TabPane tabId='4'>
                <Row>
                  <Col sm='12'>
                    <Map
                      apiKey={apiKey}
                      initialCenter={{ lat: '41.7323738', lng: '44.698428' }}
                      zoom={12}
                    />
                  </Col>
                </Row>
              </TabPane>
            </TabContent>
          </div>
        </div>
      </section>
    </Layout>
  )
}

TrainingHome.getInitialProps = async () => ({
  namespacesRequired: ['common'],
  apiKey: process.env.GOOGLE_API_KEY,
})

export default withTranslation('common')(TrainingHome)
