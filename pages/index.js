import React from 'react'
import { Layout } from 'components/layout'
import { withTranslation, Link } from 'utils/with-i18next'
import { tags, catalogItems, shares, news } from 'utils/data'
import map from 'public/images/maps.png'
import business from 'public/images/business.png'
import mapLoc from 'public/images/maps-location.png'
import humans from 'public/images/human.png'
import { WorldMap } from 'components/world-map'
import { Tabs, Tab, TabList, TabPanel, resetIdCounter } from 'react-tabs'
import FormConsultation from 'components/form/formConsultation'
import FormCommercialProposal from 'components/form/formCommercialProposal'
import { ButtonRed } from 'components/form'
import Certificates from 'components/certificates'

function Home({ t }) {
  return (
    <Layout t={t} header='home'>
      <div className='section section__one'>
        <WorldMap />
        <div className='content'>
          <h1 className='title title__section title__section--white'>
            {t('s1.t')}
          </h1>
          <h5 className='title title__description title__description--white mb-0'>
            {t('s1.d')}
          </h5>
        </div>
      </div>
      <div className='section section__two'>
        <div className='container'>
          <div className='advantages flex'>
            <div>
              <div className='advantage'>
                <div className='advantage__ico'>
                  <img src={map} alt='' />
                </div>
                <div className='advantage__body'>
                  <div className='advantage__title'>
                    {t('s2.advantages.a1.t.text1')}{' '}
                    <span>{t('s2.advantages.a1.t.text2')}</span>
                  </div>
                  <div className='advantage__description'>
                    {t('s2.advantages.a1.d')}
                  </div>
                </div>
              </div>
            </div>
            <div>
              <div className='advantage'>
                <div className='advantage__ico'>
                  <img src={business} alt='' />
                </div>
                <div className='advantage__body'>
                  <div className='advantage__title'>
                    {t('s2.advantages.a2.t.text1')}{' '}
                    <span>{t('s2.advantages.a2.t.text2')}</span>
                  </div>
                  <div className='advantage__description'>
                    {t('s2.advantages.a2.d')}
                  </div>
                </div>
              </div>
            </div>
            <div>
              <div className='advantage'>
                <div className='advantage__ico'>
                  <img src={mapLoc} alt='' />
                </div>
                <div className='advantage__body'>
                  <div className='advantage__title'>
                    {t('s2.advantages.a3.t.text1')}{' '}
                    <span>{t('s2.advantages.a3.t.text2')}</span>
                  </div>
                  <div className='advantage__description'>
                    {t('s2.advantages.a3.d')}
                  </div>
                </div>
              </div>
            </div>
            <div>
              <div className='advantage'>
                <div className='advantage__ico'>
                  <img src={humans} alt='' />
                </div>
                <div className='advantage__body'>
                  <div className='advantage__title'>
                    {t('s2.advantages.a4.t.text1')}{' '}
                    <span>{t('s2.advantages.a4.t.text2')}</span>
                  </div>
                  <div className='advantage__description'>
                    {t('s2.advantages.a4.d')}
                  </div>
                </div>
              </div>
            </div>
          </div>
          <h2 className='title title__section'>{t('s2.t')}</h2>
          <h5 className='title title__description'>{t('s2.d')}</h5>
          <div className='tabs-presentation'>
            <Tabs>
              <TabList>
                <Tab>Партнер</Tab>
                <Tab>Заказчик</Tab>
                <Tab>Вендор</Tab>
                <Tab>Соискатель</Tab>
              </TabList>
              <TabPanel>
                <div className='tab__body'>
                  <div className='tab__description'>
                    Выводим IT дилеров на новый уровень доходов за счет
                    предложения продавать эксклюзивные решения
                  </div>
                  <div className='tab__list flex'>
                    <div>
                      <div className='tab__item__index'>1</div>
                      <div className='tab__item'>
                        Поддерживаем стандарты <br /> качества товара
                      </div>
                    </div>
                    <div>
                      <div className='tab__item__index'>2</div>
                      <div className='tab__item'>
                        Помогаем внедрять маркетинговые <br />
                        мероприятия и рекламные акции
                      </div>
                    </div>
                    <div>
                      <div className='tab__item__index'>3</div>
                      <div className='tab__item'>
                        Даем IT решения которых <br />
                        нет на рынке
                      </div>
                    </div>
                  </div>
                </div>
              </TabPanel>
              <TabPanel></TabPanel>
              <TabPanel></TabPanel>
              <TabPanel></TabPanel>
            </Tabs>
          </div>
        </div>
      </div>
      <div className='section section__three'>
        <div className='container'>
          <h2 className='title title__section title__section--gray'>
            {t('s3.t')}
          </h2>
          <h4 className='title title__subtitle title__description--white'>
            {t('s3.subT')}
          </h4>
          <h5 className='title title__description title__description--white'>
            {t('s3.d')}
          </h5>
          <div className='about-info-list'>
            <div className='line__item line__item--gray'>{t('s3.text1')}</div>
            <div className='line__item line__item--gray'>{t('s3.text2')}</div>
          </div>
          <div className='after-work-steps flex'>
            <div className='after-work-steps__title'>{t('s3.steps.t')}</div>
            <div>
              <div className='step'>
                <div className='step__index'>1</div>
                <div className='step__description'>{t('s3.steps.step1')}</div>
              </div>
            </div>
            <div>
              <div className='step'>
                <div className='step__index'>2</div>
                <div className='step__description'>{t('s3.steps.step2')}</div>
              </div>
            </div>
            <div>
              <div className='step'>
                <div className='step__index'>3</div>
                <div className='step__description'>{t('s3.steps.step3')}</div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className='section section__four'>
        <div className='container'>
          <h2 className='title title__section'>{t('s4.t')}</h2>
          <div className='tabs-direction'>
            <Tabs>
              <TabList>
                <Tab>Дистрибьюторство IT решений</Tab>
                <Tab>Сервисные центры MUK</Tab>
                <Tab>Учебный центр MUK</Tab>
              </TabList>
              <TabPanel>
                <div className='tab__body'>
                  <div className='tab__title'>{t('s4.tabs.tab1.t')}</div>
                  <div className='line__item-list flex'>
                    <div className='line__item line__item--red'>
                      {t('s4.tabs.tab1.text1')}
                    </div>
                    <div className='line__item line__item--red'>
                      {t('s4.tabs.tab1.text2')}
                    </div>
                  </div>
                  <p>{t('s4.tabs.tab1.content.p1')}</p>
                  <p>{t('s4.tabs.tab1.content.p2')}</p>
                  <p>{t('s4.tabs.tab1.content.p3')}</p>
                  <p>{t('s4.tabs.tab1.content.p4')}</p>
                  <p>{t('s4.tabs.tab1.content.p5')}</p>
                </div>
              </TabPanel>
              <TabPanel></TabPanel>
              <TabPanel></TabPanel>
            </Tabs>
          </div>
          <FormConsultation t={t} />
        </div>
      </div>
      <div className='section section__five'>
        <div className='container'>
          <h2 className='title title__section title__section--white'>
            {t('s5.t')}
          </h2>
          <h5 className='title title__description title__description--white'>
            {t('s5.d')}
          </h5>
          <div className='tags flex'>
            {tags.map((item, index) => (
              <div key={index}>
                <Link href='/'>
                  <a className='tag'>{item}</a>
                </Link>
              </div>
            ))}
          </div>
          <div className='catalog-items flex nowrap'>
            {catalogItems.map((item, index) => (
              <div key={index}>
                <div className='catalog-item'>
                  <div className='catalog-item__logo'>
                    <img src={item.icon} alt='' />
                  </div>
                  <div className='catalog-item__links'>
                    {item.links.map((el, index) => (
                      <div key={index}>{el}</div>
                    ))}
                  </div>
                  <Link href='/'>
                    <a className='catalog-item__see-all'>{t('s5.btnSeeAll')}</a>
                  </Link>
                </div>
              </div>
            ))}
          </div>
        </div>
      </div>
      <div className='section section__six'>
        <div className='container'>
          <h2 className='title title__section'>{t('s6.t')}</h2>
          <h5 className='title title__description'>{t('s6.d')}</h5>
          <div className='cer__title'>{t('s6.certificates.t')}</div>
          <div className='cer__description'>{t('s6.certificates.d')}</div>
          <Certificates />
        </div>
      </div>
      <div className='section section__seven'>
        <div className='container'>
          <FormCommercialProposal t={t} />
        </div>
      </div>
      <div className='section section__eight'>
        <div className='container'>
          <h2 className='title title__section'>{t('s8.t')}</h2>
          <div className='shares'>
            {shares.map((item, index) => (
              <div key={index}>
                <div className='share flex'>
                  <div className='share__preview'>
                    <img src={item.image} alt={item.title} />
                  </div>
                  <div className='info-group__body'>
                    <div className='info-group__title'>{item.title}</div>
                    <div className='info-group__date'>
                      {t('s8.text1')}
                      {item.date}
                    </div>
                    <div className='info-group__partners'>
                      {t('s8.text2')}
                      {item.partners}
                    </div>
                    <div className='info-group__description'>
                      {item.description}
                    </div>
                  </div>
                </div>
              </div>
            ))}
          </div>
          <ButtonRed href='/'>{t('s8.btnSeeMore')}</ButtonRed>
        </div>
      </div>
      <div className='section section__nine'>
        <div className='container'>
          <h2 className='title title__section title__section--white'>
            {t('s9.t')}
          </h2>
          <div className='news flex'>
            {news.map((item, index) => (
              <div key={index}>
                <div className='new flex'>
                  <div className='info-group__body'>
                    <div className='info-group__title'>{item.title}</div>
                    <div className='info-group__date'>
                      {t('s8.text1')}
                      {item.date}
                    </div>
                    <div className='info-group__partners'>
                      {t('s8.text2')}
                      {item.partners}
                    </div>
                    <div className='info-group__description'>
                      {item.description}
                    </div>
                  </div>
                </div>
              </div>
            ))}
          </div>
          <ButtonRed href='/'>{t('s9.btnSeeMore')}</ButtonRed>
        </div>
      </div>
    </Layout>
  )
}
Home.getInitialProps = async () => {
  resetIdCounter()
  return {
    namespacesRequired: ['common'],
  }
}

export default withTranslation('common')(Home)
