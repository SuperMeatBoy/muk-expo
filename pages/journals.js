import React from 'react'
import { Layout } from 'components/layout'
import { withTranslation } from 'utils/with-i18next'
import Breadcrumbs from 'components/breadcrumbs'
import { journals, ourVendors } from 'utils/data'

const JournalsCatalog = ({ t }) => (
  <Layout>
    <section className='catalog-journals-header' />
    <section className='section section__catalog-journals'>
      <div className='container'>
        <Breadcrumbs
          className='catalog-journals-breadcrumbs'
          data={[
            { name: 'О компании', link: '/about' },
            { name: 'MUK Review' },
          ]}
        />
        <h1 className='title title__section title__section--catalog-journals'>
          {t('Выпуски журнала "MUK Review"')}
        </h1>
        <div className='row journals'>
          {journals.map((journal) => (
            <div className='col-md-3' key={journal.id}>
              <div className='journal'>
                <div className='journal__title'>{journal.issue}</div>
                <div className='journal__preview'>
                  <img src={journal.img} alt={journal.issue} />
                </div>
                <div className='journal__btns flex align-items-center justify-content-between'>
                  <a href={journal.url} download='pdf' className='link'>
                    {t('Скачать pdf')}
                  </a>
                  <a
                    href={journal.url}
                    className='btn btn__red btn__red--journal'
                    target='_blank'
                  >
                    {t('Читать онлайн')}
                  </a>
                </div>
              </div>
            </div>
          ))}
        </div>
      </div>
      <div className='container'>
        <div className='our-vendors-title'>Наши вендоры</div>
        <div className='our-vendors flex'>
          {ourVendors.map((vendor) => (
            <div key={vendor.id}>
              <img src={vendor.img} alt={vendor.alt} />
            </div>
          ))}
        </div>
      </div>
    </section>
  </Layout>
)

export function getStaticProps() {
  return {
    props: {
      namespacesRequired: ['common'],
    },
  }
}

export default withTranslation('common')(JournalsCatalog)
