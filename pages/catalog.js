import React from 'react'
import { Layout } from 'components/layout'
import { withTranslation } from 'utils/with-i18next'
import Breadcrumbs from 'components/breadcrumbs'
import { solutions } from 'utils/data.js'
import FormITConsultant from 'components/form/itConsultant'
import { SearchBlock } from 'components/form'
import { Categories } from 'components/category-list'

const SolutionsCatalog = ({ t }) => {
  return (
    <Layout>
      <section className='catalog-solutions pb-5'>
        <div className='container'>
          <Breadcrumbs data={[{ name: 'Каталог IT решений' }]} />
          <h1 className='title title__section text-white pb-3'>
            {t('Каталог IT решений')}
          </h1>
          <SearchBlock />
        </div>
      </section>
      <div className='container'>
        <Categories list={solutions} />
      </div>
      <div className='container mt-5'>
        <div className='row'>
          <FormITConsultant t={t} />
        </div>
      </div>
    </Layout>
  )
}

SolutionsCatalog.getInitialProps = async () => ({
  namespacesRequired: ['common'],
})

export default withTranslation('common')(SolutionsCatalog)
