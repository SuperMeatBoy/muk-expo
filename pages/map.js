import React from 'react'
import { WorldMap } from 'components/world-map'

export default function Map() {
  return (
    <>
      <WorldMap />
      <p>
        Lorem Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam
        facilisis, nibh sit amet tincidunt dignissim, orci eros sodales dolor,
        vel posuere nisl odio at nisi. Fusce condimentum nec sem sit amet
        blandit. Sed facilisis justo a cursus dictum. Sed vitae elit id purus
        cursus scelerisque vel id lectus. Morbi mattis id libero in lacinia. Sed
        in sollicitudin purus. Aliquam condimentum sem non neque ultricies, sit
        amet sodales ante imperdiet. Nullam quis rutrum augue. Morbi efficitur
        diam diam, sed tincidunt diam mattis auctor. Quisque tellus libero,
        semper eu varius mattis, consectetur eu ante. Phasellus elementum neque
        sem, eu consequat odio ullamcorper non. Morbi consectetur non dui ac
        sollicitudin. Cras nunc ligula, bibendum quis mauris in, pharetra
        sollicitudin nulla. Pellentesque tincidunt fringilla auctor. Mauris
        mollis pellentesque volutpat. Phasellus vel pellentesque felis, a
        dapibus nisi. Mauris tellus diam, interdum eget justo ut, eleifend
        posuere dolor. Vivamus at tempor nisi, vel facilisis velit. Proin neque
        velit, aliquam a mollis et, dignissim ac mi. Aliquam convallis magna
        commodo elementum viverra. Fusce sed lacus dictum, ullamcorper justo
        non, imperdiet arcu. Fusce maximus sem eu malesuada faucibus. In enim
        mi, dapibus sit amet laoreet ut, volutpat in turpis. Curabitur tempor
        suscipit hendrerit. Vivamus volutpat vulputate urna vel tristique. Proin
        nec mi fringilla, tempus erat et, ultrices lorem. Donec viverra nisl
        eget elementum mollis. Praesent pulvinar risus varius, bibendum lacus
        sit amet, viverra urna. Proin egestas ipsum vel hendrerit pharetra.
        Integer quis purus ac risus interdum mattis et nec urna. Maecenas et
        faucibus justo, id pellentesque mauris. Praesent vel turpis ultrices
        nibh porttitor finibus. Ut aliquet quam ut leo aliquet, sit amet
        fringilla dolor vestibulum. Quisque lobortis at metus eu consectetur.
        Mauris feugiat, elit quis congue consectetur, massa odio blandit lorem,
        sed rutrum odio nisl et magna. Vestibulum vitae urna id ante malesuada
        interdum et id orci. Fusce sit amet nulla tellus. Phasellus bibendum,
        magna eget vehicula mollis, nibh dui convallis augue, vel semper nibh
        arcu in sem. Nullam aliquam augue eu ante finibus suscipit. Aenean
        tempor neque sed mi sodales, vitae dignissim mi auctor. Integer et lorem
        id purus mattis condimentum a sed turpis. Curabitur facilisis massa eget
        consectetur aliipsum
      </p>
    </>
  )
}
