import React from 'react'
import { Layout } from 'components/layout'
import { withTranslation } from 'utils/with-i18next'
import Breadcrumbs from 'components/breadcrumbs'
import FormITConsultant from 'components/form/itConsultant'
import { PreviewCard, ProductCard } from 'components/card'
import { Manufacturer } from 'components/manufacturer'
import { listCategories } from 'utils/data'
import { Pagination } from 'components/pagination'
import { useForm } from 'react-hook-form'
import { CardTitle, Form, FormGroup, Card } from 'reactstrap'
import { Select, SearchInput, PriceRange } from 'components/form'

import vendorLogo from 'public/images/fake-data/amd-logo-white.png'

const options = [
  {
    value: 'Какая техника нужна?',
    label: 'Какая техника нужна?',
  },
  { value: 'Сервисная служба', label: 'Сервисная служба' },
]
const defaultOption = options[0]

const Category = (props) => {
  const { control, watch } = useForm()
  const watchValues = watch()

  const { t, category } = props
  const fakeArray = new Array(10).fill({ logo: vendorLogo }) // remove after add data from api
  const fakeArray1 = new Array(10).fill({ title: 'Блейд сервер' }) // remove after add data from api

  const onChange = (values) => console.log(values)
  return (
    <Layout header='white'>
      <section className='catalog pb-3'>
        <div className='container'>
          <Breadcrumbs mode='white' data={[{ name: 'Серверы' }]} />
          <h1 className='title text-dark mb-4'>Серверы</h1>
          <p className='text'>
            Обращаем ваше внимание на то, что наша компания является
            дистрибутором компьютерной техники и не продает товар в розницу
          </p>
        </div>
      </section>
      <div className='container-fluid server-bg text-white'>
        <div className='container'>
          <ul className='options pt-5 pb-5'>
            <li className='mb-2'>
              Честная цена за счет прямых поставок от производителя
            </li>
            <li className='mb-2'>Найдете цену ниже вернем разницу</li>
          </ul>
        </div>
      </div>
      <div className='container mb-5 mt-5'>
        <h2 className='mb-4 h2'>Каталог</h2>
        <div className='row'>
          {listCategories.map((item, index) => (
            <div className='col-md-4 col-sm-5 col-12' key={index}>
              <PreviewCard {...item} />
            </div>
          ))}
        </div>
      </div>
      <div className='container-fluid choose-dark mb-5 mt-5 pb-5 pt-5'>
        <div className='container'>
          <strong className='text-white d-block mb-4'>
            Выберите сервер по производителю
          </strong>
          <div className='d-flex flex-wrap justify-content-between'>
            {fakeArray.map((item, index) => (
              <div className='mb-4' key={index}>
                <Manufacturer {...item} />
              </div>
            ))}
          </div>
        </div>
      </div>
      <div className='container mb-5 mt-5'>
        <h2 className='mb-4 h2'>Подберите необходимый сервер</h2>
        <Form className='form-choose-server m-auto pb-0'>
          <FormGroup className='mb-4 pl-3 pr-3 d-flex justify-content-between align-items-center'>
            <div className='d-flex f-auto mr-5 pt-2 pb-2'>
              <Select
                name='needed'
                control={control}
                options={options}
                defaultOption={defaultOption}
                value={defaultOption}
              />
              <Select
                name='vendor'
                control={control}
                options={options}
                defaultOption={defaultOption}
                value={defaultOption}
              />
              <Select
                name='typeMount'
                control={control}
                options={options}
                defaultOption={defaultOption}
                value={defaultOption}
              />
            </div>
            <div className='d-flex align-items-center'>
              <div className='mr-4'>
                <SearchInput placeholder='Поиск по названию и артикулу' />
              </div>
              <Select
                name='typeMount'
                control={control}
                options={options}
                defaultOption={defaultOption}
                value={defaultOption}
              />
            </div>
          </FormGroup>
        </Form>
        <div className='row'>
          <div className='col-md-4 col-12'>
            <Card body>
              <CardTitle>Цена:</CardTitle>
              <PriceRange />
            </Card>
          </div>
          <div className='col-md-8 col-12'>
            <div className='row'>
              {listCategories.map((item, index) => (
                <div className='col-md-6 col-12' key={index}>
                  <ProductCard {...item} />
                </div>
              ))}
            </div>
            <div className='row justify-content-center'>
              <Pagination />
            </div>
          </div>
        </div>
      </div>
      <div className='container-fluid choose-dark mb-5 mt-5 pb-5 pt-5'>
        <div className='container'>
          <strong className='text-white d-block mb-4'>Часто ищут</strong>
          <div className='d-flex flex-wrap justify-content-between'>
            {fakeArray1.map((item, index) => (
              <div className='mb-4' key={index}>
                <Manufacturer {...item} />
              </div>
            ))}
          </div>
        </div>
      </div>
      <div className='container mt-5'>
        <FormITConsultant t={t} />
      </div>
    </Layout>
  )
}

export function getServerSideProps(ctx) {
  const category = ctx.query.category
  // Pass data to the page via props
  return { props: { category } }
}

export default withTranslation('common')(Category)
