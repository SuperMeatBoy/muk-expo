import React, { useState } from 'react'
import { Layout } from 'components/layout'
import { withTranslation } from 'utils/with-i18next'
import Breadcrumbs from 'components/breadcrumbs'
import FormITConsultant from 'components/form/itConsultant'
import { ProductCard } from 'components/card'
import { listCategories } from 'utils/data'
import { useForm } from 'react-hook-form'
import {
  TabContent,
  TabPane,
  Card,
  CardBody,
  Nav,
  NavItem,
  NavLink,
  Row,
  Col,
} from 'reactstrap'
import { ButtonRed } from 'components/form'
import clsx from 'clsx'

import server from 'public/images/fake-data/server.png'

const Product = (props) => {
  const [activeTab, setActiveTab] = useState('1')

  const toggle = (tab) => {
    if (activeTab !== tab) setActiveTab(tab)
  }

  const { control } = useForm()

  const { t, product } = props

  return (
    <Layout header='white'>
      <section className='catalog pb-3'>
        <div className='container'>
          <Breadcrumbs
            mode='white'
            data={[
              { name: 'Сервера', link: '/category/servers' },
              { name: 'Блейд сервера', link: '/category/blade-servers' },
              { name: 'Сервер PowerEdge SC440' },
            ]}
          />
          <h1 className='title text-dark mb-4'>Сервер PowerEdge SC440</h1>
          <p className='text text-gray'>Артикул: 135591137</p>
        </div>
      </section>
      <div className='container mb-3 mt-3'>
        <div className='row'>
          <div className='col-md-7'>
            <img className='img-responsive' src={server} alt='' />
          </div>
          <div className='col-md-5'>
            <Card className='p-4 card'>
              <CardBody>
                <div className='row mb-4'>
                  <div className='col-6 price'>
                    <div className='actual mb-3'>15 000 грн.</div>
                    <span className='d-inline-block old'>65 000 грн.</span>
                  </div>
                  <div className='col-6'>
                    <p className='availability mt-2'>Есть в наличии</p>
                  </div>
                </div>
                <ul className='row mb-4'>
                  <li className='col-6 mb-2'>Бренд: PowerEdge</li>
                  <li className='col-6 mb-2'>Форм фактор: Стойка</li>
                  <li className='col-6 mb-2'>Тип: Блейд сервер</li>
                  <li className='col-6 mb-2'>Гарантия: 24 месяца</li>
                </ul>
                <ButtonRed color='danger'>Купить</ButtonRed>
              </CardBody>
            </Card>
          </div>
        </div>
      </div>
      <div className='container-fluid mt-5 bg-light tabs-container'>
        <div className='container'>
          <Nav tabs>
            <NavItem>
              <NavLink
                className={clsx({ active: activeTab === '1' })}
                onClick={() => toggle('1')}
              >
                Описание
              </NavLink>
            </NavItem>
            <NavItem>
              <NavLink
                className={clsx({ active: activeTab === '2' })}
                onClick={() => toggle('2')}
              >
                Характеристики
              </NavLink>
            </NavItem>
            <NavItem>
              <NavLink
                className={clsx({ active: activeTab === '3' })}
                onClick={() => toggle('3')}
              >
                Как заказать
              </NavLink>
            </NavItem>
          </Nav>
          <TabContent activeTab={activeTab}>
            <TabPane tabId='1'>
              <Row className='pt-4 pb-5'>
                <Col sm='12'>
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                  Aenean euismod bibendum laoreet. Proin gravida dolor sit amet
                  lacus accumsan et viverra justo commodo. Proin sodales
                  pulvinar sic tempor. Sociis natoque penatibus et magnis dis
                  parturient montes, nascetur ridiculus mus. Nam fermentum,
                  nulla luctus pharetra vulputate, felis tellus mollis orci, sed
                  rhoncus pronin sapien nunc accuan eget. Lorem ipsum dolor sit
                  amet, consectetur adipiscing elit. Aenean euismod bibendum
                  laoreet. Proin gravida dolor sit amet lacus accumsan et
                  viverra justo commodo. Proin sodales pulvinar sic tempor.
                  Sociis natoque penatibus et magnis dis parturient montes,
                  nascetur ridiculus mus. Nam fermentum, nulla luctus pharetra
                  vulputate, felis tellus mollis orci, sed rhoncus pronin sapien
                  nunc accuan eget.
                </Col>
              </Row>
            </TabPane>
            <TabPane tabId='2'>
              <Row className='pt-4 pb-5'>
                <Col sm='12'>
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                  Aenean euismod bibendum laoreet. Proin gravida dolor sit amet
                  lacus accumsan et viverra justo commodo. Proin sodales
                  pulvinar sic tempor. Sociis natoque penatibus et magnis dis
                  parturient montes, nascetur ridiculus mus. Nam fermentum,
                  nulla luctus pharetra vulputate, felis tellus mollis orci, sed
                  rhoncus pronin sapien nunc accuan eget. Lorem ipsum dolor sit
                  amet, consectetur adipiscing elit. Aenean euismod bibendum
                  laoreet.
                </Col>
              </Row>
            </TabPane>
            <TabPane tabId='3'>
              <Row className='pt-4 pb-5'>
                <Col sm='12'>
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                  Aenean euismod bibendum laoreet. Proin gravida dolor sit amet
                  lacus accumsan et viverra justo commodo. Proin sodales
                  pulvinar sic tempor. Sociis natoque penatibus et magnis dis
                  parturient montes, nascetur ridiculus mus. Nam fermentum,
                  nulla luctus pharetra vulputate, felis tellus mollis orci, sed
                  rhoncus pronin sapien nunc accuan eget. Lorem ipsum dolor sit
                  amet, consectetur adipiscing elit. Aenean euismod bibendum
                  laoreet.
                  <br />
                  <br />
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                  Aenean euismod bibendum laoreet.
                </Col>
              </Row>
            </TabPane>
          </TabContent>
        </div>
      </div>
      <div className='container mb-5 mt-5'>
        <h2 className='mb-4 h2'>Похожие сервера</h2>
        <div className='row'>
          {listCategories.map((item, index) => (
            <div className='col-md-4 col-12' key={index}>
              <ProductCard {...item} mode='view' />
            </div>
          ))}
        </div>
        <div className='row justify-content-center'>
          <ButtonRed>Показать еще</ButtonRed>
        </div>
      </div>
      <div className='container mt-5'>
        <FormITConsultant t={t} />
      </div>
    </Layout>
  )
}

export async function getServerSideProps(ctx) {
  const product = ctx.query.product
  // Pass data to the page via props
  return { props: { product } }
}

export default withTranslation('common')(Product)
