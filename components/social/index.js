import React from 'react'

const Social = () => (
  <div className='social flex'>
    <a target='_blank' href='https://ok.ru/' className='ok'></a>
    <a target='_blank' href='https://vk.com/' className='vk'></a>
    <a target='_blank' href='https://www.instagram.com/' className='in'></a>
  </div>
)

export default Social
