import React, { useState } from 'react'
import { Link, i18n } from 'utils/with-i18next.js'
import { menu } from 'utils/data'
import {
  TabContent,
  TabPane,
  Nav,
  NavItem,
  NavLink,
  Row,
  Col,
} from 'reactstrap'
import { SearchInput } from 'components/form'
import clsx from 'clsx'

import Photo from 'public/images/common/close-up-portrait.jpg'
import apacer from 'public/images/fake-data/apacer.png'
import adata from 'public/images/fake-data/adata.png'
import amd from 'public/images/fake-data/amd-logo.png'

export const HeaderMenu = ({ mode = '' }) => {
  const [show, setShow] = useState(false)
  const className = clsx('flex menu', mode)
  return (
    <>
      <div className={className}>
        {menu.map((item) => {
          const onClick = (e) => {
            if (item.url === '/catalog') {
              e.preventDefault()
              setShow(!show)
            }
          }

          return (
            <div key={item.name}>
              <Link href={item.url}>
                <a onClick={onClick}>{item.name}</a>
              </Link>
            </div>
          )
        })}
      </div>
      <CatalogMenu show={show} />
    </>
  )
}

export const MobileMenu = ({ mode }) => {
  const className = clsx('flex menu', mode)
  return (
    <div className={className}>
      {menu.map((item) => {
        return (
          <div key={item.name}>
            <Link href={item.url}>
              <a>{item.name}</a>
            </Link>
          </div>
        )
      })}
    </div>
  )
}

const categories = new Array(12).fill({
  name: 'Серверное оборудование',
  list: [
    {
      name: 'Сетевые хранилища',
      link: 'network-storage',
    },
    {
      name: 'Опции к системам хранения данных',
      link: 'options-network-storage',
    },
  ],
})

const vendors = new Array(2).fill({
  apacer: {
    name: 'Apacer',
    list: new Array(4).fill({
      image: apacer,
      link: 'apacer',
    }),
  },
  adata: {
    name: 'Adata',
    list: new Array(4).fill({
      image: adata,
      link: 'adata',
    }),
  },
  amd: {
    name: 'AMD',
    list: new Array(4).fill({
      image: amd,
      link: 'amd',
    }),
  },
})

export const CatalogMenu = ({ show }) => {
  const [activeTab, setActiveTab] = useState('1')

  const toggle = (tab) => {
    if (activeTab !== tab) setActiveTab(tab)
  }

  const className = clsx('catalog-menu', show ? 'd-flex' : 'd-none')
  return (
    <Row className={className}>
      <div className='controls'>
        <Nav tabs vertical pills>
          <NavItem>
            <NavLink
              className={clsx({ active: activeTab === '1' })}
              onClick={() => toggle('1')}
            >
              {i18n.t('По категориям')}
            </NavLink>
          </NavItem>
          <NavItem>
            <NavLink
              className={clsx({ active: activeTab === '2' })}
              onClick={() => toggle('2')}
            >
              {i18n.t('По вендорам')}
            </NavLink>
          </NavItem>
        </Nav>
        <SearchInput placeholder={i18n.t('Поиск по названию')} mode='black' />
        <div className='wrapper-image mt-5'>
          <img src={Photo} alt='' className='img-responsive' />
        </div>
      </div>
      <div className='content'>
        <TabContent activeTab={activeTab}>
          <TabPane tabId='1'>
            <div className='container mt-3'>
              <Row>
                {categories.map((item, index) => (
                  <Col md='4' sm='6' key={index} className='mt-4'>
                    <strong>{item.name}</strong>
                    <ul>
                      {item.list.map((el, idx) => (
                        <li key={idx} className='mt-3'>
                          <Link href={`/category/${el.link}`}>
                            <a>{el.name}</a>
                          </Link>
                        </li>
                      ))}
                    </ul>
                  </Col>
                ))}
              </Row>
            </div>
          </TabPane>
          <TabPane tabId='2'>
            <div className='container mt-3 mb-5'>
              <Row>
                {vendors.map((item, key) => {
                  const keys = Object.keys(item)
                  return (
                    <React.Fragment key={key}>
                      {keys.map((element, index) => {
                        return (
                          <React.Fragment key={index}>
                            {item[element].list.map((el, idx) => (
                              <Col md='3' sm='4' key={idx} className='mt-5'>
                                <Link href={`/category/${el.link}`}>
                                  <a className='vendor-logo d-block'>
                                    <img
                                      src={el.image}
                                      alt=''
                                      className='img-responsive'
                                    />
                                  </a>
                                </Link>
                              </Col>
                            ))}
                          </React.Fragment>
                        )
                      })}
                    </React.Fragment>
                  )
                })}
              </Row>
            </div>
          </TabPane>
        </TabContent>
      </div>
    </Row>
  )
}
