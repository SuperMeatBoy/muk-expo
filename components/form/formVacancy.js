import React from 'react'
import { ButtonRed, Input, DragAndDrop } from 'components/form'
import { Form, FormGroup, Col } from 'reactstrap'

export function FormVacancy({ t, mode }) {
  const Button = () => (
    <>
      <ButtonRed>
        {mode === 'modal' ? 'Отправить резюме' : 'Отправить заявку на вакансию'}
      </ButtonRed>
      <p className='text-policy mt-3 d-block'>
        <span className='text-danger'>*</span> - обязательно для заполнения
      </p>
    </>
  )
  return (
    <Form>
      <FormGroup row>
        <Col md='6'>
          <Input type='text' name='name' placeholder={t('Введите ФИО*')} />
          <br />
          <Input type='text' name='email' placeholder={t('Введите e-mail*')} />
          <br />
          <Input type='text' name='phone' placeholder={t('Введите телефон*')} />
          <br />
          {mode === 'modal' && (
            <>
              <Input
                type='text'
                name='vacancy'
                placeholder={t('Какую вакансию ищете?')}
              />
              <br />
            </>
          )}
        </Col>
        <Col md='6'>
          <DragAndDrop t={t} />
          <br />
          <Input
            type='text'
            name='link'
            placeholder={t('или вставьте ссылку на резюме')}
          />
          <br />
          {mode === 'modal' && <Button />}
        </Col>
        {mode !== 'modal' && (
          <Col>
            <Button />
          </Col>
        )}
      </FormGroup>
    </Form>
  )
}
