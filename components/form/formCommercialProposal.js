import React from 'react'
import { useForm } from 'react-hook-form'
import { ButtonRed } from './button'

const FormCommercialProposal = ({ t }) => {
  const { handleSubmit, errors } = useForm()
  const onSubmit = (values) => console.log(values)

  return (
    <form
      className='form form-CommercialProposal'
      onSubmit={handleSubmit(onSubmit)}
    >
      <h2 className='title title__section'>{t('s7.t')}</h2>
      <h5 className='title title__description'>{t('s7.d')}</h5>
      <div className='flex content'>
        <div>
          <div className='manager manager__name'>{t('s7.name')}</div>
          <div className='manager manager__post'>{t('s7.post')}</div>
          <div className='input-group'>
            <input
              className='input'
              name='phone'
              placeholder='+38  (___) ___-__-__'
            />
            <span>{errors.phone && errors.phone.message}</span>
          </div>
          <div className='input-group'>
            <textarea
              className='input'
              name='description'
              placeholder={t('s7.placeholder')}
            />
            {errors.description && errors.description.message}
          </div>
          <ButtonRed type='submit'>{t('s7.btnSubmit')}</ButtonRed>
        </div>
        <div className='dot-items'>
          <div className='dot-item'>{t('s7.ul.li1')}</div>
          <div className='dot-item'>{t('s7.ul.li2')}</div>
          <div className='dot-item'>{t('s7.ul.li3')}</div>
          <div className='dot-item'>{t('s7.ul.li4')}</div>
        </div>
      </div>
    </form>
  )
}

export default FormCommercialProposal
