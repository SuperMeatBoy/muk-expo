import React, { useState, useRef, useEffect } from 'react'

export const DragAndDrop = ({ t }) => {
  const [drag, setDrag] = useState(false)
  const [files, setFiles] = useState(null)
  let dragCounter = 0

  const dropRef = useRef()

  const handleDrag = (e) => {
    e.preventDefault()
    e.stopPropagation()
  }

  const onChange = (e) => {
    const file = e.target.files || []
    file.length > 0 && setFiles([file[0].name])
  }

  const handleDragIn = (e) => {
    e.preventDefault()
    e.stopPropagation()
    dragCounter += 1
    if (e.dataTransfer.items && e.dataTransfer.items.length > 0) {
      setDrag(true)
    }
  }

  const handleDragOut = (e) => {
    e.preventDefault()
    e.stopPropagation()
    dragCounter -= 1
    if (dragCounter === 0) {
      setDrag(false)
    }
  }

  const handleDrop = (e) => {
    e.preventDefault()
    e.stopPropagation()
    setDrag(false)
    if (e.dataTransfer.files && e.dataTransfer.files.length > 0) {
      // props.handleDrop(e.dataTransfer.files)
      files
        ? setFiles(files.push(e.dataTransfer.files[0].name))
        : setFiles([e.dataTransfer.files[0].name])
      e.dataTransfer.clearData()
      dragCounter = 0
    }
  }
  useEffect(() => {
    let div = dropRef.current

    div.addEventListener('dragenter', handleDragIn)
    div.addEventListener('dragleave', handleDragOut)
    div.addEventListener('dragover', handleDrag)
    div.addEventListener('drop', handleDrop)
    return () => {
      div.removeEventListener('dragenter', handleDragIn)
      div.removeEventListener('dragleave', handleDragOut)
      div.removeEventListener('dragover', handleDrag)
      div.removeEventListener('drop', handleDrop)
    }
  }, [])

  return (
    <div className='d-a-d position-relative w-100 text-white' ref={dropRef}>
      <input type='file' onChange={onChange} />
      {drag && (
        <div className='dashed'>
          <div className='drop-here text-center'>
            <div>{t('перетащите сюда')}</div>
          </div>
        </div>
      )}
      {files && (
        <ul className='file-name d-flex align-items-center justify-content-center h-100 text-center'>
          {files.map((item, index) => (
            <li key={index}>{item}</li>
          ))}
        </ul>
      )}
      {!files && (
        <div className='d-flex align-items-center justify-content-center flex-column h-100 text-white text-center'>
          <span className='preview'>Приложить резюме</span>
          <span>перетащите сюда или нажмите</span>
        </div>
      )}
    </div>
  )
}
