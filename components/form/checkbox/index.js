import React from 'react'
import { FormGroup, Label, Input } from 'reactstrap'

export function Checkbox({ children }) {
  return (
    <FormGroup className='simple-checkbox' check>
      <Label check>
        <Input type='checkbox' />
        <span>{children}</span>
      </Label>
    </FormGroup>
  )
}

export function CustomCheckbox({ children }) {
  return (
    <FormGroup className='custom-checkbox'>
      <Label check>
        <Input type='checkbox' />
        <span>{children}</span>
      </Label>
    </FormGroup>
  )
}
