import React from 'react'
import { Link } from 'utils/with-i18next.js'
import Icon from 'components/icons'

const ButtonRed = ({ children, href, className = '', ...props }) => {
  return (
    <>
      {href ? (
        <Link href={href}>
          <a className={`custom-btn custom-btn__red ${className}`} {...props}>
            {children}
          </a>
        </Link>
      ) : (
        <button
          className={`custom-btn custom-btn__red ${className}`}
          {...props}
        >
          {children}
        </button>
      )}
    </>
  )
}

const ButtonPartner = ({ children, href, className = '', ...props }) => {
  return (
    <Link href={href}>
      <a className={`custom-btn custom-btn__partner ${className}`} {...props}>
        <Icon component='user' />
        {children}
      </a>
    </Link>
  )
}

export { ButtonRed, ButtonPartner }
