import React from "react";
import { useForm } from "react-hook-form"

const FormSearch = ({ t }) => {
  const { handleSubmit, errors } = useForm();
  const onSubmit = values => console.log(values);

  return (
    <form className='search' onSubmit={handleSubmit(onSubmit)}>
      <input type="text" name="search" placeholder={t('header.placeholderSearch')} />
      {errors.username && errors.username.message}
    </form>
  )
}

export default FormSearch