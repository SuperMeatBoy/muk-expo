import React from 'react'
import { Input as InputField } from 'reactstrap'

export const Input = (props) => {
  const { name, ...rest } = props
  return (
    <div className='input-group'>
      <InputField className='input' name={name} invalid={false} {...rest} />
    </div>
  )
}
