import React from 'react'
import { useForm } from 'react-hook-form'

export const Textarea = (props) => {
  const { errors } = useForm()
  const { error } = props
  return (
    <div className='input-group'>
      <textarea className='input' {...props} />
      <span>{errors[error] && errors[error].message}</span>
    </div>
  )
}
