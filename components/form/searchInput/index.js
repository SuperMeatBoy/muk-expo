import React from 'react'
import { InputGroup, InputGroupAddon, Input, Button } from 'reactstrap'
import searchIcon from 'public/images/icons/search.svg'
import discover from 'public/images/icons/discover.png'
import { input } from './search.module.scss'

export function SearchInput({ placeholder = '', mode }) {
  return (
    <InputGroup>
      <Input placeholder={placeholder} className={input} />
      <InputGroupAddon addonType='append'>
        <Button
          type='button'
          color={mode === 'black' ? '' : 'danger'}
          className={'rounded-0'}
        >
          <img height='23' src={mode ? discover : searchIcon} alt='' />
        </Button>
      </InputGroupAddon>
    </InputGroup>
  )
}
