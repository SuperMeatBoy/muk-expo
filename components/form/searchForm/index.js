import React from 'react'
import { useForm, Controller } from 'react-hook-form'
import { Col, Form, FormGroup } from 'reactstrap'
import { Input, Select } from 'components/form'

const options = [
  { value: 'Вендор', label: 'Вендор' },
  { value: 'Вендор 2', label: 'Вендор 2' },
]
const defaultOption = options[0]

export function SearchBlock() {
  const { control, handleSubmit } = useForm()
  const onSubmit = (values) => console.log(values)

  return (
    <Form
      className='form form-search pt-2 pb-2'
      onSubmit={handleSubmit(onSubmit)}
    >
      <FormGroup row className='mb-0 pl-3 pr-3'>
        <Col sm={6}>
          <Select
            name='theme'
            control={control}
            options={options}
            defaultOption={defaultOption}
            value={defaultOption}
          />
        </Col>
        <Col sm={6}>
          <div className='search'>
            <Controller
              as={Input}
              name='search'
              type='tel'
              placeholder='Поиск по названию'
              defaultValue=''
              control={control}
            />
          </div>
        </Col>
      </FormGroup>
    </Form>
  )
}
