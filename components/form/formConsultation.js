import React from 'react'
import { useForm } from 'react-hook-form'
import { ButtonRed } from './button'

const FormConsultation = ({ t }) => {
  const { handleSubmit, register, errors } = useForm()
  const onSubmit = (values) => console.log(values)

  return (
    <form className='form form-Consultation' onSubmit={handleSubmit(onSubmit)}>
      <h2 className='title title__section'>{t('s4.form.t')}</h2>
      <div className='manager__box'>
        <div className='manager manager__name'>{t('s4.form.name')}</div>
        <div className='manager manager__post'>{t('s4.form.post')}</div>
      </div>
      <div className='input-group'>
        <input
          className='input'
          name='phone'
          placeholder='+38  (___) ___-__-__'
        />
        <span>{errors.phone && errors.phone.message}</span>
      </div>
      <div className='input-group'>
        <textarea
          className='input'
          name='description'
          placeholder={t('s4.form.placeholder')}
        />
        {errors.description && errors.description.message}
      </div>
      <ButtonRed type='submit'>{t('s4.form.btnSubmit')}</ButtonRed>
    </form>
  )
}
export default FormConsultation
