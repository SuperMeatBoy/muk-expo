import React from 'react'
import { useForm } from 'react-hook-form'
import { ButtonRed } from './button'

const FormFooter = ({ t }) => {
  const { handleSubmit, errors } = useForm()
  const onSubmit = (values) => console.log(values)

  return (
    <form className='footer__form' onSubmit={handleSubmit(onSubmit)}>
      <div className='footer__subscribe'>{t('footer.subscribe')}</div>
      <div className='input-group'>
        <input
          className='input'
          type='email'
          name='email'
          placeholder={t('footer.placeholderEmail')}
        />
        <span>{errors.phone && errors.phone.message}</span>
      </div>
      <ButtonRed type='submit'>{t('footer.btnSubmit')}</ButtonRed>
    </form>
  )
}
export default FormFooter
