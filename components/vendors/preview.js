import React from 'react'
import styles from './vendors.module.scss'
import { Link } from 'utils/with-i18next'

export function VendorPreview({ link = '/', logo, title, name, description }) {
  return (
    <div className={`${styles.card} d-flex`}>
      <div className={`${styles.image} text-center`}>
        <Link href={`/vendor/${link}`}>
          <a>
            <img src={logo} alt={title} />
            <span>{title}</span>
          </a>
        </Link>
      </div>
      <p>
        <Link href={`/vendor/${link}`}>
          <a>
            <strong>{name}</strong>
          </a>
        </Link>
        {description}
      </p>
    </div>
  )
}
