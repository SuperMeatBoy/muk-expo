import React from 'react'
import Logo from 'public/images/logo-footer.png'
import { withTranslation, Link } from 'utils/with-i18next'
import { linkNews, linkAbout } from 'utils/data'
import FormFooter from '../form/formFooter'
import Social from '../social'

function Footer({ t }) {
  return (
    <footer className='footer'>
      <div className='container flex'>
        <div>
          <img className='footer__logo img-responsive' src={Logo} alt='' />
          <Link href='/'>
            <a className='text-brown'>{t('footer.privacyPolicy')}</a>
          </Link>
          <Link href='/'>
            <a className='text-brown'>{t('footer.termsOfUse')}</a>
          </Link>
          <div className='text-brown'>{t('footer.copyright')}</div>
        </div>
        <div>
          <div className='footer__links-title'>{t('footer.tNews')}</div>
          {linkNews.map((item) => (
            <Link href='/' key={item}>
              <a className='footer__link'>{item}</a>
            </Link>
          ))}
        </div>
        <div>
          <div className='footer__links-title'>{t('footer.tAbout')}</div>
          {linkAbout.map(({ name, link = '/' }, index) => (
            <Link href={link} key={index}>
              <a className='footer__link'>{name}</a>
            </Link>
          ))}
        </div>
        <div>
          <FormFooter t={t} />
        </div>
        <div className='footer__contact flex'>
          <a className='footer__phone' href={`tel: ${t('footer.p1')}`}>
            {t('footer.p1')}
          </a>
          <a className='footer__phone' href={`tel: ${t('footer.p1')}`}>
            {t('footer.p1')}
          </a>
          <a className='footer__email' href={`mailto: ${t('footer.e1')}`}>
            {t('footer.e1')}
          </a>
          <a className='footer__callback' href='/'>
            {t('footer.callback')}
          </a>
          <Social />
        </div>
      </div>
    </footer>
  )
}

export default withTranslation('common')(Footer)
