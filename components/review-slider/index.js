import React from 'react'
import Carousel from 'react-multi-carousel'
import {
  responsiveCard,
  reviewEducationalCenter,
} from 'utils/data'

const ReviewSlider = ({t, title}) => {
  const sliderOptionsOne = responsiveCard()
  return (
    <section className='section section__review-slider'>
      <div className='container'>
        <h2 className='title title__section title__section--white fz-30'>
          {title}
        </h2>
        <Carousel
          additionalTransfrom={0}
          ssr
          infinite={true}
          arrows={true}
          draggable={false}
          responsive={sliderOptionsOne}
          minimumTouchDrag={false}
          focusOnSelect={false}
        >
          {reviewEducationalCenter.map((item, idx) => (
            <div key={idx}>
              <div className='review-training-card flex'>
                <div className='review-training-card__preview'>
                  <img src={item.image} alt='' />
                </div>
                <div className='review-training-card__body'>
                  <div className='review-training-card__title-box'>
                    <div className='review-training-card__title fz-24'>
                      {t('Заголовок отзыва')}
                    </div>
                    <div className='review-training-card__text fz-18'>
                      {item.title}
                    </div>
                  </div>
                  <div className='review-training-card__title-box'>
                    <div className='review-training-card__title'>
                      {t('Цель прихода на курс')}
                    </div>
                    <div className='review-training-card__text'>
                      {item.description}
                    </div>
                  </div>
                  <div className='review-training-card__title-box'>
                    <div className='review-training-card__title'>
                      {t('Отзыв о курсе')}
                    </div>
                    <div className='review-training-card__text'>
                      {item.review}
                    </div>
                  </div>
                </div>
              </div>
            </div>
          ))}
        </Carousel>
      </div>
    </section>
  )
}

export default ReviewSlider