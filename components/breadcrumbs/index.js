import React from 'react'
import { Breadcrumb, BreadcrumbItem } from 'reactstrap'
import { withTranslation, Link } from 'utils/with-i18next'
import clsx from 'clsx'

const Breadcrumbs = ({ t, className = '', data = [], mode = '' }) => {
  const classNames = clsx(mode === 'white' ? 'text-dark' : 'text-white')
  return (
    <div className={className}>
      <Breadcrumb>
        <BreadcrumbItem>
          <Link href='/'>
            <a className={classNames}>{t('breadcrumbs.home')}</a>
          </Link>
        </BreadcrumbItem>
        {data.map((item, index) => {
          if (item.link) {
            return (
              <BreadcrumbItem key={index}>
                <Link href={item.link}>
                  <a className={classNames}>{item.name}</a>
                </Link>
              </BreadcrumbItem>
            )
          }
          return (
            <BreadcrumbItem key={index} active>
              {item.name}
            </BreadcrumbItem>
          )
        })}
      </Breadcrumb>
    </div>
  )
}

export default withTranslation('common')(Breadcrumbs)
