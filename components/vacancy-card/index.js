import React from 'react'
import { Card, CardBody } from 'reactstrap'
import { Link } from 'utils/with-i18next'
import { SharingButton } from 'components/sharing-button'

export function VacancyCard({
  title = '',
  location = '',
  link = '/',
  responsibilities = [],
  t,
}) {
  return (
    <Card className='vacancy-card w-100'>
      <CardBody className='w-100'>
        <div className='row'>
          <div className='col-md-3 col-12'>
            {title}
            <br />
            {location}
          </div>
          <div className='col-md-1 col-0'>
            <div className='line-v' />
          </div>
          <div className='col-md-8 col-12'>
            <div>{t('Обязанности')}</div>
            <ul className='list'>
              {responsibilities.map((item, index) => (
                <li className='list-item' key={index}>
                  {item}
                </li>
              ))}
            </ul>
            <div className='d-flex justify-content-between flex-wrap'>
              <div className='mr-3'>
                <Link href={`/vacancies/${link}`}>
                  <a>{t('Подробнее')}</a>
                </Link>
              </div>
              <div className='d-flex align-items-center'>
                Расскажите друзьям о вакансии: <SharingButton />
              </div>
            </div>
          </div>
        </div>
      </CardBody>
    </Card>
  )
}
