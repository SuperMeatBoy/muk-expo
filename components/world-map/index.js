import React, { useRef, useEffect, useState } from 'react'
import { worldMapConfig } from './map-config'
import { countriesPath } from './countries'
import { TouchScroll } from './touch-scroll'
import { responsiveCard } from 'utils/data'
import Carousel from 'react-multi-carousel'
import clsx from 'clsx'

export function WorldMap() {
  const [markers, setMarkers] = useState(null)
  const [showBanner, setShowBanner] = useState(false)
  const [countryInfo, setCountryInfo] = useState(null)
  const [lineRect, setLineRect] = useState(null)

  const worldMapRef = useRef()
  const worldMapBgRef = useRef()
  const worldMapSVG = useRef()
  const bannerLineRef = useRef()
  const worldMapScrollRef = useRef()
  const carouselRef = useRef()
  const countries = worldMapConfig.countries

  const sliderOptionsOne = responsiveCard()

  useEffect(() => {
    const viewer = new TouchScroll()
    viewer.init({
      id: 'worldmapscroll',
      draggable: true,
      wait: false,
    })
    if (worldMapSVG.current) {
      calcMapBgWidth()
      setMarkersRect()
    }
    window.addEventListener(
      'resize',
      () => {
        setTimeout(() => {
          reload()
        }, 100)
      },
      false
    )
    return () => {
      window.removeEventListener('resize', reload)
    }
  }, [])

  useEffect(() => {
    let elem = worldMapScrollRef.current
    let value = elem.offsetWidth

    let scrollIndex
    switch (true) {
      case value > 1440:
        scrollIndex = 5
        break
      case value > 1200:
        scrollIndex = 4
        break
      case value > 992:
        scrollIndex = 3
        break
      default:
        scrollIndex = 2
        break
    }

    elem.scrollTo(elem.scrollWidth / scrollIndex, 0)
  }, [])

  function reload() {
    calcMapBgWidth()
    setMarkersRect()
  }

  function buildLine(id) {
    let target = document.getElementById('marker' + id)
    let bannerLineRect = bannerLineRef.current.getBoundingClientRect()

    let x1 = bannerLineRect.left
    let y1 = bannerLineRect.bottom + window.scrollY
    let x2 = target.getBoundingClientRect().left + target.offsetWidth / 2
    let y2 =
      target.getBoundingClientRect().top +
      target.offsetHeight / 2 +
      window.scrollY
    setLineRect({ x1, x2, y1, y2 })
  }

  function onMouseEnter(event) {
    event.stopPropagation()
    event.preventDefault()
    if (window.matchMedia('(hover: hover)').matches) {
      const country = countries[event.target.id]
      setCountryInfo(country)
      setShowBanner(true)

      buildLine(event.target.id)

      toggleSelected(event.target.id, true)
    }
  }

  function onMouseLeave(event) {
    setShowBanner(false)
    setCountryInfo(null)
    setLineRect(null)
    toggleSelected(event.target.id, false)
  }

  function onClick(event) {
    event.stopPropagation()
    if (!window.matchMedia('(hover: hover)').matches) {
      const country = countries[event.target.id]
      setCountryInfo(country)
      setShowBanner(true)

      toggleSelected(event.target.id, true)
      carouselRef.current.goToSlide(country.id + 1)
      let svg = worldMapSVG.current
      const countriesArr = svg && Array.from(svg.childNodes)
      countriesArr.forEach((i) => {
        if (event.target.id === i.id) {
          i.classList.add('selected')
        } else if (event.target.id !== i.id) {
          i.classList.remove('selected')
        }
      })
    }
  }

  function onSlide({ currentSlide, nextSlide }) {
    let country =
      countriesPath.find((i) => {
        if (nextSlide === 1) {
          return i.number === 16
        } else {
          return i.number === nextSlide - 1
        }
      }) || {}

    let svg = worldMapSVG.current
    const countriesArr = svg && Array.from(svg.childNodes)
    countriesArr.forEach((i, ind) => {
      if (currentSlide - 1 === ind + 1) {
        i.classList.add('selected')
      } else {
        i.classList.remove('selected')
      }
    })

    setShowBanner(true)
    // toggleSelected(country.id, true)
  }

  function toggleSelected(id, boolean) {
    let markerId = `marker${id}`
    markers.forEach((i) => (i.selected = false))
    let item = markers.find((item) => item.id === markerId)
    item.selected = boolean

    let arr = markers
    arr.splice(arr.indexOf(item), 1, item)
    setMarkers(arr)
  }

  function setMarkersRect() {
    let svg = worldMapSVG.current
    const countriesArr = svg && Array.from(svg.childNodes)
    let markersArr = []
    countriesArr.forEach((element) => {
      const attr = element.getAttribute('id')
      const rect = element.getBoundingClientRect()

      let markerX = Math.round(
        rect.left +
          (rect.right - rect.left) / 2 -
          svg.getBoundingClientRect().left
      )

      let markerY = Math.round(
        rect.top +
          (rect.bottom - rect.top) / 2 -
          svg.getBoundingClientRect().top
      )

      markersArr.push({
        id: `marker${attr}`,
        style: {
          left: `${markerX}px`,
          top: `calc(${markerY}px + ${worldMapConfig.offsetTop}vh)`,
        },
        selected: false,
      })
    })
    setMarkers(markersArr)
  }

  function calcMapBgWidth() {
    document.getElementById('worldmapsvg').style.height =
      worldMapConfig.zoom + 'vh'
    document.getElementById('worldmapbg').style.width =
      worldMapSVG.current.getBoundingClientRect().width + 'px'
    document.getElementById('worldmapbg').style.backgroundPosition =
      'center top ' + parseInt(worldMapConfig.offsetTop) + 'vh'
    document.getElementById('worldmapsvg').style.top =
      worldMapConfig.offsetTop + 'vh'
  }

  let Marker = ({ selected, style, id }) => (
    <span
      id={id}
      className={clsx('map__marker', selected && 'selected')}
      style={style}
    />
  )

  let Banner = ({ showBanner, countryInfo = {}, mobile }) => {
    const { address, phones, email } = countryInfo || {}
    return (
      <div
        className={clsx(
          'map__banner custom-card text-white',
          showBanner && 'show',
          mobile && 'mobile'
        )}
      >
        {countryInfo && (
          <>
            {address && (
              <div className='custom-card__title text-white'>{address}</div>
            )}

            {phones && (
              <div className='custom-card__title text-white d-flex'>
                <span className='d-block mr-2'>Телефон: </span>
                <div>
                  {phones.map((item, index) => (
                    <a href={`tel${item}`} key={index} className='d-block'>
                      {item}
                    </a>
                  ))}
                </div>
              </div>
            )}
            {email && (
              <div className='custom-card__title text-white'>
                <span>Электронная почта:</span> {email}
              </div>
            )}
          </>
        )}
        {!mobile && <span ref={bannerLineRef} className='map__banner-line' />}
      </div>
    )
  }

  let LineToBanner = ({ show, rect }) => (
    <line id='map__line' {...rect} className={clsx(show && 'show')} />
  )

  return (
    <div id='worldmap' ref={worldMapRef}>
      <div id='worldmapscroll' ref={worldMapScrollRef}>
        <div id='worldmapbg' ref={worldMapBgRef}>
          <svg
            id='worldmapsvg'
            ref={worldMapSVG}
            width='100%'
            height='100%'
            viewBox='0 0 1009.6727 665.96301'
            preserveAspectRatio='xMidYMin slice'
            draggable
          >
            {countriesPath.map(({ path, id, title }) => (
              <path
                d={path}
                onMouseEnter={onMouseEnter}
                onMouseLeave={onMouseLeave}
                onClick={onClick}
                id={id}
                key={id}
                title={title}
              />
            ))}
          </svg>
        </div>
        {markers &&
          markers.map((item, index) => <Marker key={index} {...item} />)}
      </div>
      <Banner showBanner={showBanner} countryInfo={countryInfo} />
      <Carousel
        ref={carouselRef}
        additionalTransfrom={0}
        ssr
        infinite={true}
        arrows={false}
        draggable={true}
        showDots
        responsive={sliderOptionsOne}
        minimumTouchDrag={100}
        focusOnSelect={false}
        sliderClass='pb-3'
        beforeChange={(nextSlide, { currentSlide }) =>
          onSlide({ nextSlide, currentSlide })
        }
      >
        {Object.values(countries).map((item, index) => (
          <Banner showBanner={true} countryInfo={item} key={index} mobile />
        ))}
      </Carousel>
      <svg id='map__svg'>
        <LineToBanner rect={lineRect} show={showBanner} />
      </svg>
    </div>
  )
}
