import React from 'react'
import { ButtonRed } from '../form/button'

const FormConsultation = ({ email }) => {
  return (
    <form className='form-consultation'>
      <div className='form-consultation__title'>
        Получите более подробную информацию о
        сотрудничестве отправив письмо на почту
        ads@muk.com.ua или оставив заявку в форме
      </div>
      <input type="text" name='phone' className='input' placeholder='Введите телефон'/>
      {email && <input type="email" name='email' className='input' placeholder='Введите e-mail'/>}
      <ButtonRed type='submit'>Узнать подробности сотрудничества</ButtonRed>
      <div className="ps">Позвоним в течении 15 минут и обсудим условия</div>
    </form>
  )
}

export default FormConsultation