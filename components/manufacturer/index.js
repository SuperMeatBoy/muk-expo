import React from 'react'
import { manufacturer } from './manufacturer.module.scss'

export function Manufacturer(props = {}) {
  const { logo, title } = props
  return (
    <div className={manufacturer}>
      {logo && <img src={logo} alt='' className='img-responsive' />}
      {title && <p className='text-white m-0'>{title}</p>}
    </div>
  )
}
