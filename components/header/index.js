import React, { useState } from 'react'
import Head from 'next/head'
import { Link, withTranslation } from 'utils/with-i18next.js'

import { ChangeLanguage } from 'components/language'
import { ChangeCity } from 'components/city'
import Logo from '../logo'
import FormSearch from '../form/formSearch'
import { ButtonPartner, ButtonRed } from 'components/form/button'
import { HeaderMenu, MobileMenu } from '../menu'
import clsx from 'clsx'

function Header({ t, mode = '' }) {
  const [toggleMenu, setToggleMenu] = useState(false)
  const className = clsx(
    'header',
    toggleMenu && 'menu-open',
    mode === 'home' ? 'header--home' : mode
  )

  return (
    <>
      <Head>
        <title>Title</title>
        <link rel='icon' href='/favicon.ico' />
      </Head>
      <header className={className}>
        <div className='flex header__mobile'>
          <Logo />
        </div>
        <div className='flex header__mobile header__mobile--button'>
          <div className='oval' />
          <div
            className='toggle-menu'
            onClick={() => setToggleMenu(!toggleMenu)}
          >
            <span></span>
            <span></span>
            <span></span>
          </div>
        </div>
        <div className='container flex header__body'>
          <div className='flex header__top'>
            <ChangeCity mode={mode} />
            <ChangeLanguage mode={mode} />
            <Link href='/'>
              <a className='reg'>Регистрация</a>
            </Link>
          </div>
          <div className='flex header__middle'>
            <div className='flex'>
              <Logo mode={mode} />
              <div className='logo-description'>{t('header.dLogo')}</div>
            </div>
            <FormSearch t={t} />
            <ButtonRed className='btn__md'>{t('header.btnCallback')}</ButtonRed>
            <div className='header__contact flex'>
              <a href={`tel: ${t('header.p1')}`}>{t('header.p1')}</a>
              <a href={`tel: ${t('header.p2')}`}>{t('header.p2')}</a>
            </div>
            <ButtonPartner href='/'>{t('header.btnPartner')}</ButtonPartner>
          </div>
          <div className='flex header__bottom'>
            <HeaderMenu mode={mode} />
          </div>
        </div>
        <div className='header__mobile--body'>
          <div>
            <FormSearch t={t} />
          </div>
          <div>
            <MobileMenu mode={mode} />
          </div>
          <div className='contact-group'>
            <div className='header__contact flex'>
              <a href={`tel: ${t('header.p1')}`}>{t('header.p1')}</a>
              <a href={`tel: ${t('header.p2')}`}>{t('header.p2')}</a>
            </div>
            <ButtonRed className='btn__md'>{t('header.btnCallback')}</ButtonRed>
          </div>
          <div className='flex header-button-group'>
            <ButtonPartner href='/'>{t('header.btnPartner')}</ButtonPartner>
            <Link href='/'>
              <a className='reg'>Регистрация</a>
            </Link>
          </div>
        </div>
      </header>
    </>
  )
}

export default withTranslation('common')(Header)
