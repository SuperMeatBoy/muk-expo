import React, { useState } from 'react'
import { i18n } from 'utils/with-i18next'

import dynamic from 'next/dynamic'
const Dropdown = dynamic(() => import('react-dropdown'), {
  ssr: false,
})

const options = [
  { value: 'ru', label: 'Рус' },
  { value: 'en', label: 'EN' },
]

export function ChangeLanguage() {
  const [lang, setLang] = useState(i18n.language)

  const _onSelect = (e) => {
    setLang(e.value)
    i18n.changeLanguage(e.value)
  }

  return (
    <Dropdown
      options={options}
      onChange={(value) => _onSelect(value)}
      value={lang}
      placeholder='Select language'
    />
  )
}
