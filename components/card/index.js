import React from 'react'
import { Link } from 'utils/with-i18next'
import { ProductModal } from 'components/modals'
import {
  Card,
  CardImg,
  CardText,
  CardBody,
  CardTitle,
  CardSubtitle,
  Button,
} from 'reactstrap'
import clsx from 'clsx'
import { ButtonRed, Input } from 'components/form'

export function PreviewCard({ name, description, image, link = '' }) {
  return (
    <Card className='mb-5 pt-4 pb-4 pr-3 pl-3 card'>
      <Link href={link}>
        <a>
          <CardImg top width='100%' src={image} alt='' />
        </a>
      </Link>
      <CardBody>
        <CardTitle>
          <Link href={link}>
            <a>
              <strong className='mt-4 mb-3 d-block'>{name}</strong>
            </a>
          </Link>
        </CardTitle>
        <CardText>{description}</CardText>
      </CardBody>
    </Card>
  )
}

export function ProductCard(props) {
  const { name, description, image, link, mode } = props
  return (
    <Card className='card mb-5 pt-4 pb-4 pr-3 pl-3'>
      <div className='sale'>Хит продаж</div>
      <Link href={link}>
        <a>
          <CardImg top width='100%' src={image} alt='' />
        </a>
      </Link>
      <CardBody>
        {mode !== 'view' && (
          <CardSubtitle className='availability'>Есть в наличии</CardSubtitle>
        )}
        <CardTitle>
          <Link href={link}>
            <a>
              <strong className='mt-4 mb-3 d-block'>{name}</strong>
            </a>
          </Link>
        </CardTitle>
        <CardText>{description}</CardText>
        {mode !== 'view' && (
          <ul className='details mb-4'>
            <li className='mb-2'>Бренд: PowerEdge</li>
            <li className='mb-2'>Форм фактор: Стойка</li>
            <li className='mb-2'>Тип: Блейд сервер</li>
          </ul>
        )}
        <div className='row price mb-4 align-items-center'>
          <div className='col-7'>
            <div className='actual'>15 000 грн.</div>
          </div>
          <div className='col-5'>
            <span className='d-inline-block old'>65 000 грн.</span>
          </div>
        </div>
        <div className='d-flex align-items-center justify-content-between'>
          {mode !== 'view' && <ModalContent {...props} />}
          <Link href={link}>
            <a>
              <Button
                color='link'
                className={clsx(
                  'btn text-reset pl-0',
                  mode === 'view' && 'pl-0'
                )}
              >
                Подробнее
              </Button>
            </a>
          </Link>
        </div>
      </CardBody>
    </Card>
  )
}

export function CourseCard(props) {
  const { name, date, image, link, mode, category, price, oldPrice } = props
  const fullLink = `/training/${category}/${link}`

  return (
    <Card className='card card-course mb-5 pt-4 pb-4'>
      <div className='sale'>Хит продаж</div>
      <Link href={fullLink}>
        <a>
          <CardImg top width='100%' src={image} alt='' />
        </a>
      </Link>
      <CardBody>
        <CardTitle>
          <Link href={fullLink}>
            <a>
              <strong className='mt-1 mb-3 d-block'>{name}</strong>
            </a>
          </Link>
        </CardTitle>
        <CardText>Дата начала: {date}</CardText>
        <div className='row price mb-4 align-items-center'>
          <div className='col-7'>
            <div className='actual'>{price}</div>
          </div>
          <div className='col-5'>
            <span className='d-inline-block old'>{oldPrice}</span>
          </div>
        </div>
        <div className='d-flex align-items-center justify-content-between'>
          {mode !== 'view' && <ModalContent {...props} />}
          <Link href={fullLink}>
            <a>
              <Button
                color='link'
                className={clsx(
                  'btn text-reset pl-0',
                  mode === 'view' && 'pl-0'
                )}
              >
                Подробнее
              </Button>
            </a>
          </Link>
        </div>
      </CardBody>
    </Card>
  )
}

const ModalContent = ({
  image = '',
  name,
  price,
  oldPrice,
  list,
  date,
  description,
}) => {
  return (
    <ProductModal buttonLabel='Купить'>
      <div className='title h3'>Для уточнения деталей и заказа</div>
      <div className='row'>
        <div className='col-6'>
          <div className='mb-3'>
            оставьте телефон в форме
            <br />
            <span className='font-weight-bold'>
              Позвоним в течении 10 минут
            </span>
          </div>
          <Input className='input mb-3' placeholder={'Введите телефон'} />
          <ButtonRed>Уточнить детали из купить</ButtonRed>
          <p className='policy mt-2'>
            Нажимая на кнопку, вы даете согласие на обработку персональны данных
            и соглашаетесь с Пользовательским соглашением
          </p>
        </div>
        <div className='col-6'>
          <Card className='card p-0 shadow-none'>
            <div className='font-weight-bold'>{name}</div>
            <div className='row mb-4'>
              <div className='col-6'>
                <p className='article-product mt-2'>
                  Артикул товара: 135591137
                </p>
                {description && <div>{description}</div>}
                {list && (
                  <ul className='mb-4'>
                    <li className='mb-2'>Бренд: PowerEdge</li>
                    <li className='mb-2'>Форм фактор: Стойка</li>
                    <li className='mb-2'>Тип: Блейд сервер</li>
                  </ul>
                )}
                {date && <div>Дата: {date}</div>}
              </div>
              <div className='col-6'>
                <CardImg top width='100%' src={image} alt='' />
              </div>
            </div>

            <div className='price'>
              <span className='d-inline-block old'>{oldPrice}</span>
              <div className='actual mt-3'>{price}</div>
            </div>
          </Card>
        </div>
      </div>
    </ProductModal>
  )
}
