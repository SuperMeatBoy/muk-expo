import { Map, Marker, GoogleApiWrapper } from 'google-maps-react'
let apiKey

const containerStyle = {
  position: 'relative',
  width: '100%',
  height: '100%',
}

const MapContainer = ({
  apiKey,
  google,
  zoom = 17,
  initialCenter = { lat: '50.4228138', lng: '30.4631008' },
}) => {
  apiKey = apiKey

  const onMarkerClick = (value) => console.log(value)
  return (
    <div
      style={{ maxWidth: '980px', height: '400px' }}
      className='overflow-hidden'
    >
      <Map
        google={google}
        containerStyle={containerStyle}
        initialCenter={initialCenter}
        zoom={zoom}
      >
        <Marker onClick={onMarkerClick} name={'Current location'} />
      </Map>
    </div>
  )
}

export default GoogleApiWrapper({
  apiKey,
})(MapContainer)
