const NextI18Next = require('next-i18next').default
const data = require('next/config').default().publicRuntimeConfig
const { localeSubpaths } = data

const localeSubpathVariations = {
  none: {},
  foreign: {
    en: 'en',
  },
  all: {
    en: 'en',
    ru: 'ru',
  },
}

module.exports = new NextI18Next({
  defaultLanguage: 'ru',
  otherLanguages: ['en'],
  localePath: 'public/locales',
  localeSubpaths: localeSubpathVariations[localeSubpaths],
})
