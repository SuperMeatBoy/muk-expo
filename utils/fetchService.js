import useSWR from 'swr'
import fetch from 'isomorphic-unfetch'

const fetcher = (url) => fetch(url).then((r) => r.json())

export function exampleGetClientUrl() {
  const { data, mutate } = useSWR('/api/news', fetcher)
  return [data, { mutate }]
}

export class ApiClient {
  constructor(baseUrl) {
    this.baseUrl =
      baseUrl ||
      (process.browser && window.location.origin) ||
      'http://localhost:3000'
  }

  _request({ method, url, config = {} }) {
    return new Promise((resolve, reject) => {
      let payload = {
        method,
        headers: {
          'Content-Type': 'application/json',
          ...config.headers,
        },
        ...config,
      }

      fetch(this.baseUrl + url, payload)
        .then((response) => {
          if (response.status === 401) {
            throw new Error()
          }
          return response.json()
        })
        .then((json) => {
          if (json) {
            resolve(json)
          } else {
            reject('Error')
          }
        })
        .catch((err) => {
          reject(err)
        })
    })
  }

  get(url, params, config) {
    return this._request({
      method: 'GET',
      url,
      data: params,
      config,
    })
  }

  post(url, body, config) {
    return this._request({
      method: 'POST',
      url,
      data: body,
      config,
    })
  }

  put(url, body, config) {
    return this._request({
      method: 'PUT',
      url,
      data: body,
      config,
    })
  }

  patch(url, body, config) {
    return this._request({
      method: 'PATCH',
      url,
      data: body,
      config,
    })
  }

  delete(url, body, config) {
    return this._request({
      method: 'DELETE',
      url,
      data: body,
      config,
    })
  }
}
